package com.anth.redtable.main;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.anth.redtable.function.Val;

public class BottomMenu{
	private final String TAG = "RedTable_BottomMenu";
	private Context context;

	/**
	 * 메뉴가 열렸을 때 이동거리
	 */
	//	public static final int length = 300;
	public static final int length = (int) (Val.screenWidth/1.6);

	/**
	 * 가장 좌측버튼 동작시간
	 */
	public static final int duration = 100;

	/**
	 * 하위 메뉴 동작 시간
	 */
	public static final int sub_duration = 300;

	/**
	 * 하위 메뉴 선택시 동작 시간
	 */
	public static final int sub_select_duration = 200;

	/**
	 * 하위 메뉴 동작시 각 버튼간의 시간 Gap
	 */
	public static final int sub_offset = 30;
	
	int OrgTop = -1;
	int OrgLeft = -1;

	/**
	 * 가장왼쪽 버튼을 눌렀을때 애니메이션 처리
	 * 버튼이 45도 회전
	 */
	public void startMenuAnimation(ArrayList<BottomMenuButton> buttons, ImageView Btn, boolean open, Context mContext){
		/*Animation rotate;

		if(open){
			rotate = new RotateAnimation(
					0, 90
					, Animation.RELATIVE_TO_SELF, 0.5f
					, Animation.RELATIVE_TO_SELF, 0.5f);
		}else{
			rotate = new RotateAnimation(
					90, 0
					, Animation.RELATIVE_TO_SELF, 0.5f
					, Animation.RELATIVE_TO_SELF, 0.5f);
		}

		rotate.setInterpolator(AnimationUtils.loadInterpolator(mContext, android.R.anim.bounce_interpolator));
		rotate.setFillAfter(true);
		rotate.setDuration(duration);
		Btn.startAnimation(rotate);
		 */
		for(int i=0; i<buttons.size(); i++){
			startSubButtonAnimation(buttons, mContext, i, open);
		}
	}

	/**
	 * 하위메뉴가 애니메이션
	 * 회전하며 이동
	 * 효과를 위해 Interpolater사용
	 */
	public void startSubButtonAnimation(final ArrayList<BottomMenuButton> buttons, final Context mContext, final int index, final boolean open){
		final BottomMenuButton view = buttons.get(index);

		//float endX = length * FloatMath.cos((float)(Math.PI * 1/2 * (index)/(buttons.size()-1)));
		//float endY = length * FloatMath.sin((float)(Math.PI * 1/2 * (index)/(buttons.size()-1)));
		float endX = length * (index+1)/(buttons.size()-1) + 15;

		AnimationSet animation = new AnimationSet(false);
		Animation translate;
		Animation rotate = new RotateAnimation(
				0, 360
				, Animation.RELATIVE_TO_SELF, 0.5f
				, Animation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(sub_duration);
		rotate.setRepeatCount(0);
		//rotate.setInterpolator(AnimationUtils.loadInterpolator(mContext, android.R.anim.accelerate_interpolator));

		if(open){
			translate = new TranslateAnimation(0, endX, 0, 0);
			translate.setDuration(sub_duration);
			translate.setInterpolator(AnimationUtils.loadInterpolator(mContext, android.R.anim.overshoot_interpolator));
			translate.setStartOffset(sub_offset*index);
			animation.addAnimation(rotate);
			
			view.setOffset(endX, 0);
		} else{
			// 뷰 이동안할때
//			translate = new TranslateAnimation(endX, 0, 0, 0);
			translate = new TranslateAnimation(0, -endX, 0, 0);
			translate.setDuration(sub_duration);
			translate.setInterpolator(AnimationUtils.loadInterpolator(mContext, android.R.anim.anticipate_interpolator));
			translate.setStartOffset(sub_offset*(buttons.size()-(index+1)));

			view.setOffset(-endX, 0);
		}

		animation.setFillAfter(false);
		animation.addAnimation(rotate);
		animation.addAnimation(translate);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationEnd(Animation animation) {
				movePathButton(buttons, mContext, index, open);
				
			}
		});

		view.startAnimation(animation);
	}
	
	private void movePathButton(ArrayList<BottomMenuButton> buttons, Context mContext, int index, boolean open){
		BottomMenuButton curBtn = (BottomMenuButton) buttons.get(index);
		float endX = length * (index+1)/(buttons.size()-1) + 15;
		// 시작버튼보다 멀게 시작할 때
		//if(index > 0) endX += 10;
//		FrameLayout.LayoutParams curLayoutParams = (FrameLayout.LayoutParams) curBtn.getLayoutParams();
//		LinearLayout.LayoutParams curLayoutParams = (LinearLayout.LayoutParams) curBtn.getLayoutParams();
//		RelativeLayout.LayoutParams curLayoutParams = (RelativeLayout.LayoutParams) curBtn.getLayoutParams();
//		if(this.OrgTop == -1){
//			this.OrgTop = curLayoutParams.topMargin;
//			this.OrgLeft = curLayoutParams.leftMargin;
//		}
//		Log.d(TAG, "Button " + index + " ori leftMargin : " + curLayoutParams.leftMargin);
//		Log.d(TAG, "ButtonPaddingLeft : " + curBtn.getPaddingLeft());
		if(open){
//			curLayoutParams.topMargin = this.OrgTop;
//			curLayoutParams.leftMargin = this.OrgLeft + (int)endX;
//			curBtn.setLayoutParams(curLayoutParams);
			
//			curBtn.setPadding(this.OrgLeft + (int)endX, 0, 0, 0);
			curBtn.offsetLeftAndRight((int)endX);
			
//			int width = curBtn.getWidth();
//			int height = curBtn.getHeight();
			
//			curBtn.setWidth(width);
//			curBtn.setHeight(height);
		} else{
//			curLayoutParams.topMargin = this.OrgTop;
//			curLayoutParams.leftMargin = this.OrgLeft;
//			curBtn.setLayoutParams(curLayoutParams);
			
//			curBtn.setPadding(this.OrgLeft, 0, 0, 0);
			curBtn.offsetLeftAndRight(-(int)endX);
			
//			curBtn.setPadding(150, 0, 0, 0);
		}
//		Log.d(TAG, "Button " + index + " set leftMargin : " + curLayoutParams.leftMargin);
		
	}
}

