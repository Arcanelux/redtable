package com.anth.redtable.main;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.anth.redtable.CategoryResult;
import com.anth.redtable.DetailWebView;
import com.anth.redtable.R;
import com.anth.redtable.function.DeleteTemp;
import com.anth.redtable.function.GetData;
import com.anth.redtable.function.SimpleDialog;
import com.anth.redtable.function.Val;
import com.anth.redtable.mypage.MyPageTask;
import com.anth.redtable.search.Search;
import com.anth.redtable.setting.Setting;
import com.anth.redtable.values.Region;
import com.anth.redtable.values.RegionArrayAdapter;
import com.anth.redtable.values.Restaurant;
import com.anth.redtable.values.RestaurantArrayAdapter;

public class RedTableActivity extends Activity implements OnClickListener, OnItemClickListener{
	private final String TAG = "RedTable_RedTableActivity";

	private Context context;
	private ImageView likeBtn, categoryBtn, searchBtn, arrowCur, arrow1, arrow2;		//arrow123(카테고리 옆 빨간화살표)
	private FrameLayout leftMenu1, leftMenu2, leftMenuCur;
	private boolean isMenuOpened = false;
	private boolean isLeftMenuOpened = false;
	private BottomMenu menu;
	private View leftMenu, leftMenuOutSide, viewListView1, viewListView2, CurListView;
	private Animation aniShow, aniHide, listOpen, listClose;

	private ArrayList<Region> regionList1, regionList2, curRegionList;
	private RegionArrayAdapter regionAA1 = null;
	private RegionArrayAdapter regionAA2 = null;
	private ListView listView1, listView2, curListView;

	private ArrayList<Restaurant> resList;
	private ListView mainListView;
	private RestaurantArrayAdapter resAA;

	/** 레스토랑 리스트 생성 **/
	private String area_code1;
	private String area_code2;
	private Integer page = 1;
	private Integer limit = 10;

	/** 하위 메뉴 버튼 리스트 */
	private ArrayList<BottomMenuButton> buttons;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);
		context = this;

		/** 화면크기 **/
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Val.screenWidth = display.getWidth();
		Val.screenHeight = display.getHeight();


		/** 지역관련 변수들 **/
		String regions1[] = { "가락동", "가로수길", "강남역", "교대역", "구로", "노량진", "논현", "대치동", "도곡", "명일동", "목동", "반포", "발산역", "방배", "방이", "사당역", "삼성역", "서래마을", "서울대입구역", "선릉역", "신림역", "신사역", "신천역", "압구정", "양재", "여의도", "역삼역", "영등포", "잠실역", "천호", "청담" };
		String regions2[] = { "건대입구역", "광화문", "구의역", "남대문", "노원", "대학로", "동대문역", "동부이촌", "마포", "명동", "방화", "부암동", "삼청", "서대문역", "성북동", "성수역", "성신여대입구역", "숙대입구역", "시청", "신촌", "연신내", "연희동", "왕십리", "용산", "응암동", "이태원", "인사동", "장충동", "종로", "청량리", "충무로", "태릉입구역", "평창동", "홍대", "회기역", "효자동" };

		regionList1 = new ArrayList<Region>();
		regionList2 = new ArrayList<Region>();

		for(int i=0; i<regions1.length; i++){
			String regionCode1 = "LA000";
			String regionCode2 = "LA" + String.format("%03d", i+1);
			Region aRegion = new Region(regions1[i], regionCode1, regionCode2);
			regionList1.add(aRegion);
		}
		for(int i=0; i<regions2.length; i++){
			String regionCode1 = "LB000";
			String regionCode2 = "LB" + String.format("%03d", i+1);
			Region aRegion = new Region(regions2[i], regionCode1, regionCode2);
			regionList2.add(aRegion);
		}

		// 임시로 건대입구 지정
		Val.CUR_REGION = regionList2.get(0);
		area_code1 = Val.CUR_REGION.getRegionCode1();
		area_code2 = Val.CUR_REGION.getRegionCode2();


		/** 메인 리스트뷰 설정 **/
		resList = new ArrayList<Restaurant>();
		resList = GetData.makeResList(context, resList, area_code1, area_code2, page, limit, Val.makeMode);

		mainListView = (ListView) findViewById(R.id.mainListView);
		resAA = new RestaurantArrayAdapter(this, R.layout.restaurant_cell, resList);

		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View footer = inflater.inflate(R.layout.footer, null);
		footer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GetData.makeResList(context, resList, area_code1, area_code2, ++page, limit, Val.addMode);
				resAA.notifyDataSetChanged();
			}
		});
		mainListView.addFooterView(footer);

		mainListView.setAdapter(resAA);
		mainListView.setTag("mainListView");
		mainListView.setFadingEdgeLength(0);
		mainListView.setOnItemClickListener(this);



		/** 슬라이딩 메뉴 관련 변수들 **/
		listView1 = (ListView) findViewById(R.id.listView1);
		regionAA1 = new RegionArrayAdapter(this, R.layout.listlayout_leftmenu, regionList1);
		listView1.setAdapter(regionAA1);
		listView1.setTag("listView1");
		listView1.setOnItemClickListener(this);
		listView1.setFadingEdgeLength(0);
		listView1.setVisibility(View.GONE);

		listView2 = (ListView) findViewById(R.id.listView2);
		regionAA2 = new RegionArrayAdapter(this, R.layout.listlayout_leftmenu, regionList2);
		listView2.setAdapter(regionAA2);
		listView2.setTag("listView2");
		listView2.setOnItemClickListener(this);
		listView2.setFadingEdgeLength(0);
		listView2.setVisibility(View.GONE);

		arrow1 = (ImageView) findViewById(R.id.arrow1);
		arrow2 = (ImageView) findViewById(R.id.arrow2);
		//arrowCur = (ImageView) findViewById(R.id.arrowCur);

		leftMenu = findViewById(R.id.leftMenu);
		leftMenu.setVisibility(View.GONE);
		leftMenuOutSide = findViewById(R.id.leftMenuOutside);
		aniShow = AnimationUtils.loadAnimation(this, R.anim.left_in);
		aniHide = AnimationUtils.loadAnimation(this, R.anim.left_out);
		listOpen = AnimationUtils.loadAnimation(this, R.anim.menu_open);
		listClose = AnimationUtils.loadAnimation(this, R.anim.menu_close);

		CurListView = findViewById(R.id.listViewCur);
		CurListView.setVisibility(View.GONE);

		leftMenu1 = (FrameLayout) findViewById(R.id.leftMenu1);
		leftMenu1.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				if(listView1.getVisibility() == View.GONE){
					listView2.setVisibility(View.GONE);
					listView1.setVisibility(View.VISIBLE);
					//viewListView1.startAnimation(listOpen);
					arrow1.setImageResource(R.drawable.arrow3);
					arrow2.setImageResource(R.drawable.arrow2);
				}else if(listView1.getVisibility() == View.VISIBLE){
					//viewListView1.startAnimation(listClose);
					listView1.setVisibility(View.GONE);
					arrow1.setImageResource(R.drawable.arrow2);
				}
			}
		});

		leftMenu2 = (FrameLayout) findViewById(R.id.leftMenu2);
		leftMenu2.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				if(listView2.getVisibility() == View.GONE){
					listView1.setVisibility(View.GONE);
					listView2.setVisibility(View.VISIBLE);
					//viewListView1.startAnimation(listOpen);
					arrow2.setImageResource(R.drawable.arrow3);
					arrow1.setImageResource(R.drawable.arrow2);
				}else if(listView2.getVisibility() == View.VISIBLE){
					//viewListView1.startAnimation(listClose);
					listView2.setVisibility(View.GONE);
					arrow2.setImageResource(R.drawable.arrow2);
				}
			}
		});

		leftMenuOutSide.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				if(isLeftMenuOpened){
					leftMenu.startAnimation(aniHide);
					leftMenu.setVisibility(View.INVISIBLE);
					isLeftMenuOpened = false;
				}
			}
		});

		categoryBtn = (ImageView) findViewById(R.id.categoryBtn);
		categoryBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				if(!isLeftMenuOpened){
					leftMenu.setVisibility(View.VISIBLE);
					leftMenu.startAnimation(aniShow);
					isLeftMenuOpened = true;
				}else{/*
        			leftMenu.startAnimation(aniHide);
        			leftMenu.setVisibility(View.INVISIBLE);
        			isLeftMenuOpened = false;*/
				}
			}
		});

		searchBtn = (ImageView) findViewById(R.id.searchBtn);
		searchBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(RedTableActivity.this, Search.class);
				startActivity(intent);
			}
		});

		/** 애니메이션 버튼 관련 변수들 **/
		context = this;
		menu = new BottomMenu();
		buttons = new ArrayList<BottomMenuButton>();
		int buttonList[] = { R.id.checkin, R.id.photo, R.id.review, R.id.scrape, R.id.setting };

		for(int buttonName : buttonList){
			BottomMenuButton button = (BottomMenuButton) findViewById(buttonName);
			button.setOnClickListener(this);
			buttons.add(button);
		}

		likeBtn = (ImageView)findViewById(R.id.likeBtn);
		likeBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				if(!isMenuOpened){
					isMenuOpened = true;
				} else{
					isMenuOpened = false;
				}
				menu.startMenuAnimation(buttons, likeBtn, isMenuOpened, context);
			}
		});

		initialize();
	}

	/** 스플래시(로딩화면)표시하는 것과 초기화를 동시에 진행시키기위해 쓰레드처리  */
	private void initialize(){
		InitializationRunnable init = new InitializationRunnable();
		new Thread(init).start();
	}

	/** 초기화 작업 처리 */
	class InitializationRunnable implements Runnable{
		public void run(){
			//여기서부터 초기화 작업 처리
		}
	}

	/** 애니메이션 버튼 클릭 이벤트 **/
	@Override
	public void onClick(View v) {
		if(Val.FACEBOOK.isSessionValid() || v.getId()==R.id.setting){
			switch(v.getId()){
			case R.id.checkin:
				//			Toast.makeText(context, "CheckIn Clicked", Toast.LENGTH_SHORT).show();
				Intent checkIntent = new Intent(this, Search.class);
				checkIntent.putExtra("To", Val.TO_CHECKIN);
				startActivity(checkIntent);
				break;
			case R.id.photo:
				//			Toast.makeText(context, "Photo Clicked", Toast.LENGTH_SHORT).show();
				Intent photoIntent = new Intent(this, Search.class);
				photoIntent.putExtra("To", Val.TO_CAMERA);
				startActivity(photoIntent);
				break;
			case R.id.review:
				//			Toast.makeText(context, "Review Clicked", Toast.LENGTH_SHORT).show();
				Intent reviewIntent = new Intent(this, Search.class);
				reviewIntent.putExtra("To", Val.TO_REVIEW);
				startActivity(reviewIntent);
				break;
			case R.id.scrape:
				//			Toast.makeText(context, "Scrape Clicked", Toast.LENGTH_SHORT).show();
				new MyPageTask(context).execute();
				break;
			case R.id.setting:
				//			Toast.makeText(context, "Setting Clicked", Toast.LENGTH_SHORT).show();
				Intent settingIntent = new Intent(this, Setting.class);
				startActivity(settingIntent);
				break;
			}
		} else{
			SimpleDialog.makeRequireLoginDialog(context);
		}
	}

	/** 리스트뷰 아이템 클릭 이벤트 **/
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		String curViewName = (parent.getTag()).toString();

		if(curViewName.equals("mainListView")){
			/** 어댑터에서 findViewById로 TextView의 내용을 뽑아오는 예제 **/
			Val.CUR_RES = resList.get(position);
			Intent intent = new Intent(RedTableActivity.this, DetailWebView.class);
			startActivity(intent);
		}
		if(curViewName.equals("listView1") || curViewName.equals("listView2")){
			leftMenu.setVisibility(View.INVISIBLE);
			isLeftMenuOpened = false;
			if(curViewName.equals("listView1")){
				Val.CUR_REGION = regionList1.get(position);
			} else{
				Val.CUR_REGION = regionList2.get(position);
			}
			Intent intent = new Intent(RedTableActivity.this, CategoryResult.class);
			startActivity(intent);
		}
	}

	@Override
	protected void onDestroy() {
		DeleteTemp.deletePhoto();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		DeleteTemp.deletePhoto();
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		System.exit(0);
	}




}