/**
 * Author : Lee HanYeong
 * File Name : SendPhotoTask.java
 * Created Date : 2012. 9. 25.
 * Description
 */
package com.anth.redtable.search;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.anth.redtable.function.PostData;
import com.anth.redtable.function.Val;

public class SendReviewTask extends AsyncTask<Void, Integer, Void>{
	private Context mContext;
	private ProgressDialog mProgressDialog;

	private ArrayList<String> photoNameList;
	private String review;
	int type = 0;

	public SendReviewTask(Context context){
		mContext = context;
		type = Val.TO_CHECKIN;
	}
	public SendReviewTask(Context context, ArrayList<String> photoNameList){
		mContext = context;
		this.photoNameList = photoNameList;
		type = Val.TO_CAMERA;
	}
	public SendReviewTask(Context context, ArrayList<String> photoNameList, String review){
		mContext = context;
		this.photoNameList = photoNameList;
		this.review = review;
		type = Val.TO_REVIEW;
	}

	@Override
	protected void onPreExecute() {
		mProgressDialog = new ProgressDialog(mContext);
		switch(type){
		case Val.TO_CHECKIN:
			mProgressDialog.setMessage("체크인 중...");
			break;
		case Val.TO_CAMERA:
			mProgressDialog.setMessage("사진 전송중...");
			break;
		case Val.TO_REVIEW:
			mProgressDialog.setMessage("리뷰 전송중...");
			break;
		}
		mProgressDialog.show();
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}

	@Override
	protected Void doInBackground(Void... params) {
		switch(type){
		case Val.TO_CHECKIN:
			PostData.sendCheckin(Val.EMAIL, Val.CUR_RES.getRest_id(), mContext);
			break;
		case Val.TO_CAMERA:
			PostData.sendPhoto(Val.EMAIL, Val.CUR_RES.getRest_id(), photoNameList, mContext);
			break;
		case Val.TO_REVIEW:
			PostData.sendReview(Val.EMAIL, Val.CUR_RES.getRest_id(), review, photoNameList, mContext);
			break;
		}

		return null;
	}

	@Override
	protected void onCancelled() {
		mProgressDialog.dismiss();
		super.onCancelled();
	}

	@Override
	protected void onPostExecute(Void result) {
		mProgressDialog.dismiss();
		switch(type){
		case Val.TO_CHECKIN:
//			PostData.sendCheckin(Val.EMAIL, Val.CUR_RES.getRest_id(), mContext);
			if(PostData.SEND_SUCCESS) {
				Toast.makeText(mContext, "체크인 완료", Toast.LENGTH_SHORT).show();
			} else{
				Toast.makeText(mContext, "체크인 실패", Toast.LENGTH_SHORT).show();
			}
			((Activity)mContext).finish();
			break;
		case Val.TO_CAMERA:
//			PostData.sendPhoto(Val.EMAIL, Val.CUR_RES.getRest_id(), photoNameList, mContext);
			if(PostData.SEND_SUCCESS) {
				Toast.makeText(mContext, "사진 업로드 완료", Toast.LENGTH_SHORT).show();
			} else{
				Toast.makeText(mContext, "사진 업로드 실패", Toast.LENGTH_SHORT).show();
			}
			((Activity)mContext).finish();
			break;
		case Val.TO_REVIEW:
//			PostData.sendReview(Val.EMAIL, Val.CUR_RES.getRest_id(), review, photoNameList, mContext);
			if(PostData.SEND_SUCCESS) {
				Toast.makeText(mContext, "리뷰 업로드 완료", Toast.LENGTH_SHORT).show();
			} else{
				Toast.makeText(mContext, "리뷰 업로드 실패", Toast.LENGTH_SHORT).show();
			}
			((Activity)mContext).finish();
			break;
		}
		
		super.onPostExecute(result);
	}
}
