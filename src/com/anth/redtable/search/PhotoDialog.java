/**
 * Author : Lee HanYeong
 * File Name : PhotoDialog.java
 * Created Date : 2012. 9. 25.
 * Description
 */
package com.anth.redtable.search;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anth.redtable.R;
import com.anth.redtable.review.Review;

public class PhotoDialog extends Dialog implements android.view.View.OnClickListener{
	private Context mContext;
	private TextView titleText;
	private Button btn1, btn2;
	
	private ArrayList<String> photoNameList;
	
	public PhotoDialog(ArrayList<String> photoNameList, Context context){
		super(context);
		mContext = context;
		setContentView(R.layout.dialog_photo);
		
		this.photoNameList = photoNameList;
		
		titleText = (TextView) findViewById(R.id.tvPhotoDialogTitle);
		btn1 = (Button) findViewById(R.id.btnPhotoDialog1);
		btn2 = (Button) findViewById(R.id.btnPhotoDialog2);
		
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnPhotoDialog1:
			/**
			 * 리뷰작성으로 넘어갈 시
			 * CamearD 클래스에서 redtabletemp5.jpg로 저장된 파일이름을
			 * redtabletemp0.jpg로 바꿔준 후, Review클래스로 넘어감
			 * 넘어가며 Dialog를 호출한 액티비티를 finish
			 */
//			Toast.makeText(mContext, "btn", Toast.LENGTH_SHORT).show();
			File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/redtabletemp" + 5 + ".jpg");
			file.renameTo(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/redtabletemp" + 0 + ".jpg"));
			Intent intent = new Intent((Activity)mContext, Review.class);
			intent.putExtra("from", "photoDialog");
			((Activity)mContext).startActivity(intent);
			((Activity)mContext).finish();
			dismiss();
			break;
		case R.id.btnPhotoDialog2:
			dismiss();
			new SendReviewTask(mContext, photoNameList).execute();
			break;
		}
	}
}
