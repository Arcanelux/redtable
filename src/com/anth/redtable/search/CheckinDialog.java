/**
 * Author : Lee HanYeong
 * File Name : CheckinDialog.java
 * Created Date : 2012. 9. 24.
 * Description
 */
package com.anth.redtable.search;

import java.util.Currency;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.anth.redtable.R;
import com.anth.redtable.function.PostData;
import com.anth.redtable.function.Val;

public class CheckinDialog extends Dialog implements android.view.View.OnClickListener {
	private Context mContext;
	private TextView titleText;
	private Button btn1, btn2, btn3;
	
	public CheckinDialog(Context context){
		super(context);
		mContext = context;
		setContentView(R.layout.dialog_checkin);
		
		titleText = (TextView) findViewById(R.id.tvCheckinDialogTitle);
		btn1 = (Button) findViewById(R.id.btnCheckinDialog1);
		btn2 = (Button) findViewById(R.id.btnCheckinDialog2);
		btn3 = (Button) findViewById(R.id.btnCheckinDialog3);
		
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnCheckinDialog1:
			break;
		case R.id.btnCheckinDialog2:
			break;
		case R.id.btnCheckinDialog3:
			dismiss();
			new SendReviewTask(mContext).execute();
			break;
		}
	}
}
