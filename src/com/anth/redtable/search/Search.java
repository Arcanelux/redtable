/**
 * Author : Lee HanYeong
 * File Name : Search.java
 * Created Date : 2012. 9. 24.
 * Description
 */
package com.anth.redtable.search;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.anth.redtable.DetailWebView;
import com.anth.redtable.R;
import com.anth.redtable.camera.CameraD;
import com.anth.redtable.function.GetData;
import com.anth.redtable.function.SimpleDialog;
import com.anth.redtable.function.Val;
import com.anth.redtable.review.BitmapEdit;
import com.anth.redtable.review.Review;
import com.anth.redtable.values.Restaurant;
import com.anth.redtable.values.RestaurantArrayAdapter;

public class Search extends Activity implements OnItemClickListener{
	private final String TAG = "RedTable_Search";
	private Context mContext;
	private ListView mListView;
	private RestaurantArrayAdapter mResAA;
	private ImageView ivBackBtn, ivHomeBtn, ivSearchBtn;
	private EditText etSearch;
	
	private ArrayList<Restaurant> resList;
	private int from = 0;
	private boolean searchListAddMode = false;
	
	/** 레스토랑 리스트 생성(JSON) **/
	String area_code1;
	String area_code2;
	Integer page = 1;
	Integer limit = 10;
	
	String keyword;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.search);
		mContext = this;
		
		Intent intent = getIntent();
		from = intent.getIntExtra("To", 0);
		
		mListView = (ListView) findViewById(R.id.lvSearch);
		ivBackBtn = (ImageView) findViewById(R.id.ivSearchBackBtn);
		ivHomeBtn = (ImageView) findViewById(R.id.ivSearchHomeBtn);
		ivSearchBtn = (ImageView) findViewById(R.id.ivSearchSearchBtn);
		etSearch = (EditText) findViewById(R.id.etSearch);
		
		ivBackBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { finish(); }
		});
		ivHomeBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { finish(); }
		});
		ivSearchBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				keyword = etSearch.getText().toString();
				if(searchListAddMode==false){
					page = 1;
					resList.clear();
//					mResAA.notifyDataSetChanged();
				}
				GetData.makeResSearchList(mContext, resList, page++, limit, searchListAddMode, keyword);
				searchListAddMode = true;
				mResAA.notifyDataSetChanged();
			}
		});
		
		/** 레스토랑 리스트 생성(JSON) **/
		area_code1 = Val.CUR_REGION.getRegionCode1();
		area_code2 = Val.CUR_REGION.getRegionCode2();
		
		resList = new ArrayList<Restaurant>();
		resList = GetData.makeResList(mContext, resList, area_code1, area_code2, page, limit, Val.makeMode);

		if(resList.size()==0){
			SimpleDialog.makeNoneResDialog(mContext);
		} else{
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View footer = inflater.inflate(R.layout.footer, null);
			footer.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.d(TAG, "Footer");
					if(searchListAddMode==false){
						GetData.makeResList(mContext, resList, area_code1, area_code2, ++page, limit, Val.addMode);
					} else{
						GetData.makeResSearchList(mContext, resList, page++, limit, !searchListAddMode, keyword);
					}
					mResAA.notifyDataSetChanged();
				}
			});
			mListView.addFooterView(footer);
		}
		
		mResAA = new RestaurantArrayAdapter(this, R.layout.restaurant_cell, resList);
		mListView.setAdapter(mResAA);
		mListView.setOnItemClickListener(this);
		mListView.setFadingEdgeLength(0);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		switch(from){
		case Val.NONE:
			Val.CUR_RES = resList.get(position);
			Intent noneIntent = new Intent(Search.this, DetailWebView.class);
			startActivity(noneIntent);
			break;
		case Val.TO_CHECKIN:
//			Toast.makeText(mContext, resList.get(position).getRest_id() + " Clicked\n" + "From : " + from, Toast.LENGTH_SHORT).show();
			Val.CUR_RES = resList.get(position);
			CheckinDialog chkDialog = new CheckinDialog(mContext);
			chkDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			chkDialog.show();
			break;
		case Val.TO_CAMERA:
//			Toast.makeText(mContext, resList.get(position).getRest_id() + " Clicked\n" + "From : " + from, Toast.LENGTH_SHORT).show();
			Val.CUR_RES = resList.get(position);
			Intent toCameraIntent = new Intent(Search.this, CameraD.class);
			startActivityForResult(toCameraIntent, Val.TO_CAMERA);
			break;
		case Val.TO_REVIEW:
//			Toast.makeText(mContext, resList.get(position).getRest_id() + " Clicked\n" + "From : " + from, Toast.LENGTH_SHORT).show();
			Val.CUR_RES = resList.get(position);
			Intent toReviewIntent = new Intent(Search.this, Review.class);
			startActivity(toReviewIntent);
			finish();
			break;
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK){
			switch(requestCode){
			case Val.TO_CAMERA:
				String number = Integer.toString(data.getIntExtra("no", 5));
				String fileName = "/redtabletemp" + number + ".jpg";
				File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + fileName);
				try {
					Bitmap photoBitmap = Media.getBitmap(getContentResolver(), Uri.fromFile(file));
					/** 카메라에서 찍은 사진일경우 revisionBitmapFromCamera메소드 실행 **/
					if(data.getIntExtra("from", 0) == Val.PICK_FROM_CAMERA) { photoBitmap = BitmapEdit.revisionBitmapFromCamera(photoBitmap); }
					/** 업로드용 이미지 편집&파일저장 */
					Bitmap finalResize = BitmapEdit.makeResizeBitmap(photoBitmap, Val.RESIZE_HEIGHT2);
					FileOutputStream fos = new FileOutputStream(file);
					finalResize.compress(CompressFormat.JPEG, 70, fos);
					fos.close();
					
					ArrayList<String> photoNameList = new ArrayList<String>();
					photoNameList.add(fileName);
					
					PhotoDialog mPhotoDialog = new PhotoDialog(photoNameList, mContext);
					mPhotoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
					mPhotoDialog.show();
				} catch (FileNotFoundException e) {
					Log.d(TAG, "Cannot find File - Search");
					e.printStackTrace();
				} catch (IOException e) {
					Log.d(TAG, "find File Error");
					e.printStackTrace();
				}
			}
		}
	}
}
