/**
 * Author : Lee HanYeong
 * File Name : MyPageTask.java
 * Created Date : 2012. 9. 27.
 * Description
 */
package com.anth.redtable.mypage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.anth.redtable.DetailWebView;
import com.anth.redtable.function.PostData;

public class MyPageTask extends AsyncTask<Void, Integer, Void>{
	private Context mContext;
	private ProgressDialog mProgressDialog;
	
	private String myPageUrl;

	public MyPageTask(Context context){
		mContext = context;
	}
	
	@Override
	protected void onPreExecute() {
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage("접속중...");
		mProgressDialog.show();
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		myPageUrl = PostData.getMyPageUrl();
		return null;
	}

	@Override
	protected void onCancelled() {
		mProgressDialog.dismiss();
		super.onCancelled();
	}

	@Override
	protected void onPostExecute(Void result) {
		mProgressDialog.dismiss();
		Intent intent = new Intent((Activity)mContext, DetailWebView.class);
		intent.putExtra("url", myPageUrl);
		((Activity)mContext).startActivity(intent);
		super.onPostExecute(result);
	}
}
