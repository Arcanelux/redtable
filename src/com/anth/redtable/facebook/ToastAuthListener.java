/**
 * Author : Lee HanYeong
 * File Name : AuthListener.java
 * Created Date : 2012. 9. 25.
 * Description
 */
package com.anth.redtable.facebook;

import android.content.Context;
import android.widget.Toast;

import com.anth.redtable.facebook.SessionEvents.AuthListener;
import com.anth.redtable.function.Val;
import com.anth.redtable.setting.Setting.SampleRequestListener;

public class ToastAuthListener implements AuthListener {
	private Context mContext;
	public ToastAuthListener(Context context){
		mContext = context;
	}
	public void onAuthSucceed() {
		//            mText.setText("You have logged in! ");
		//            mRequestButton.setVisibility(View.VISIBLE);
		//            mUploadButton.setVisibility(View.VISIBLE);
		//            mPostButton.setVisibility(View.VISIBLE);
		Toast.makeText(mContext, "로그인 되었습니다", Toast.LENGTH_SHORT).show();
//		tvLoginText.setText("로그인 되었습니다");
		//Val.ASYNC_RUNNER.request("me", new SampleRequestListener());
	}

	public void onAuthFail(String error) {
		//            mText.setText("Login Failed: " + error);
		Toast.makeText(mContext, "로그인에 실패하였습니다", Toast.LENGTH_SHORT).show();
	}
}