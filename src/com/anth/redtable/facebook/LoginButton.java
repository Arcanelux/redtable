/*
 * Copyright 2010 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.anth.redtable.facebook;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.anth.redtable.R;
import com.anth.redtable.facebook.SessionEvents.AuthListener;
import com.anth.redtable.facebook.SessionEvents.LogoutListener;
import com.anth.redtable.function.PostData;
import com.anth.redtable.function.SaveVal;
import com.anth.redtable.function.Val;
import com.anth.redtable.setting.Setting;
import com.anth.redtable.setting.Setting.SampleRequestListener;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class LoginButton extends ImageButton {
	private final String TAG = "RedTable_LoginButton";
	private Facebook mFb;
	private Handler mHandler;
	private SessionListener mSessionListener = new SessionListener();
	private String[] mPermissions;
	private Activity mActivity;
	
	private TextView tvLoginText;
	private ToggleButton btnAutoLogin;

	public LoginButton(Context context) {
		super(context);
	}

	public LoginButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LoginButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void init(final Activity activity, final Facebook fb) {
		init(activity, fb, new String[] {"email"});
	}

	public void init(final Activity activity, final Facebook fb, final String[] permissions) {
		mActivity = activity;
		mFb = fb;
		mPermissions = permissions;
		mHandler = new Handler();
		
		tvLoginText = (TextView) findViewById(R.id.tvSettingLoginText);
		btnAutoLogin = (ToggleButton) findViewById(R.id.btnSettingAutologin);
//		tvLoginText.setText()

		setBackgroundColor(Color.TRANSPARENT);
		setAdjustViewBounds(true);
		setImageResource(fb.isSessionValid() ?
				R.drawable.facebook_logout : 
					R.drawable.facebook_login);
		if(fb.isSessionValid()){
			AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(mFb);
			asyncRunner.request("me", new SampleRequestListener());
		}
		drawableStateChanged();

		SessionEvents.addAuthListener(mSessionListener);
		SessionEvents.addLogoutListener(mSessionListener);
		setOnClickListener(new ButtonOnClickListener());
	}

	private final class ButtonOnClickListener implements OnClickListener {

		public void onClick(View arg0) {
			if (mFb.isSessionValid()) {
				SessionEvents.onLogoutBegin();
				AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(mFb);
				asyncRunner.logout(getContext(), new LogoutRequestListener());
			} else {
				mFb.authorize(mActivity, mPermissions,
						new LoginDialogListener());
			}
		}
	}
	
	

	private final class LoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	private class LogoutRequestListener extends BaseRequestListener {
		public void onComplete(String response, final Object state) {
			// callback should be run in the original thread, 
			// not the background thread
			mHandler.post(new Runnable() {
				public void run() {
					SessionEvents.onLogoutFinish();
				}
			});
		}
	}

	private class SessionListener implements AuthListener, LogoutListener {

		public void onAuthSucceed() {
			if(!PostData.join()){
				SessionEvents.onLogoutBegin();
				AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(mFb);
				asyncRunner.logout(getContext(), new LogoutRequestListener());
				Val.REDTABLE_LOGIN = false;
				Log.d(TAG, "RedTable Login Failed, Facebook Login Cancel");
			} else{
				setImageResource(R.drawable.facebook_logout);
				SessionStore.save(mFb, getContext());
				Val.REDTABLE_LOGIN = true;
				Log.d(TAG, "RedTable Login Success, Facebook Login Success");
			}
			
		}

		public void onAuthFail(String error) {
		}

		public void onLogoutBegin() {           
		}

		public void onLogoutFinish() {
			SessionStore.clear(getContext());
			setImageResource(R.drawable.facebook_login);
			btnAutoLogin = (ToggleButton) mActivity.findViewById(R.id.btnSettingAutologin);
			btnAutoLogin.setChecked(false);
			SaveVal.setAutoLogin(mActivity, false);
		}
	}
	
	public class SampleRequestListener extends BaseRequestListener {
		public void onComplete(final String response, final Object state) {
			try {
				// process the response here: executed in background thread
				Log.d("Facebook-Example", "Response: " + response.toString());
				JSONObject json = Util.parseJson(response);
				final String name = json.getString("name");
				final String email = json.getString("email");
				
				
				// then post the processed result back to the UI thread
				// if we do not do this, an runtime exception will be generated
				// e.g. "CalledFromWrongThreadException: Only the original
				// thread that created a view hierarchy can touch its views."
				mActivity.runOnUiThread(new Runnable() {
					public void run() {
						//                        mText.setText("Hello there, " + name + "!");
//						Toast.makeText(mContext, "name:" + name, Toast.LENGTH_SHORT).show();
						tvLoginText = (TextView) mActivity.findViewById(R.id.tvSettingLoginText);
						tvLoginText.setText(email + mActivity.getString(R.string.login2));
						
						Log.d(TAG, email);
					}
				});
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
		}
	}

}
