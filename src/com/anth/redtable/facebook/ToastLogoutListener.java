/**
 * Author : Lee HanYeong
 * File Name : ToastLogoutListener.java
 * Created Date : 2012. 9. 25.
 * Description
 */
package com.anth.redtable.facebook;

import android.content.Context;
import android.widget.Toast;

import com.anth.redtable.facebook.SessionEvents.LogoutListener;

public class ToastLogoutListener implements LogoutListener {
	private Context mContext;
	public ToastLogoutListener(Context context){
		mContext = context;
	}
	public void onLogoutBegin() {
		Toast.makeText(mContext, "로그아웃 중입니다", Toast.LENGTH_SHORT).show();
	}

	public void onLogoutFinish() {
		Toast.makeText(mContext, "로그아웃 되었습니다", Toast.LENGTH_SHORT).show();
//		tvLoginText.setText(getString(R.string.login1));
		//            mText.setText("You have logged out! ");
		//            mRequestButton.setVisibility(View.INVISIBLE);
		//            mUploadButton.setVisibility(View.INVISIBLE);
		//            mPostButton.setVisibility(View.INVISIBLE);
	}
}