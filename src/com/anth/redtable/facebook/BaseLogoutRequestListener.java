/**
 * Author : Lee HanYeong
 * File Name : BaseLogoutListener.java
 * Created Date : 2012. 9. 27.
 * Description
 */
package com.anth.redtable.facebook;

import android.os.Handler;

public class BaseLogoutRequestListener extends BaseRequestListener {
	public void onComplete(String response, final Object state) {
		// callback should be run in the original thread, 
		// not the background thread


		SessionEvents.onLogoutFinish();


	}
}