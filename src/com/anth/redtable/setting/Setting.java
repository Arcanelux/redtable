/**
 * Author : Lee HanYeong
 * File Name : Setting.java
 * Created Date : 2012. 9. 24.
 * Description
 */
package com.anth.redtable.setting;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.anth.redtable.R;
import com.anth.redtable.facebook.BaseDialogListener;
import com.anth.redtable.facebook.BaseRequestListener;
import com.anth.redtable.facebook.LoginButton;
import com.anth.redtable.facebook.SessionEvents;
import com.anth.redtable.facebook.SessionEvents.AuthListener;
import com.anth.redtable.facebook.SessionEvents.LogoutListener;
import com.anth.redtable.facebook.SessionStore;
import com.anth.redtable.function.SaveVal;
import com.anth.redtable.function.Val;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class Setting extends Activity{
	private final String TAG = "RedTable_Setting";
	private Context mContext;

	//	private Button btnFacebook;
	private LoginButton btnFacebook;
	private ToggleButton btnAutoLogin;
	private ImageView ivBackBtn, ivHomeBtn;
	private TextView tvLoginText, tvVersion;

	private Facebook mFacebook;
	private AsyncFacebookRunner mAsyncRunner;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = this;

		setContentView(R.layout.setting);

		//		btnFacebook = (Button) findViewById(R.id.btnSettingFacebook);
		btnFacebook = (LoginButton) findViewById(R.id.btnSettingFacebook);
		btnAutoLogin = (ToggleButton) findViewById(R.id.btnSettingAutologin);
		btnAutoLogin.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				if(!btnAutoLogin.isChecked()){
						SaveVal.setAutoLogin(mContext, false);
				} else{
					if(mFacebook.isSessionValid()){
						SaveVal.setAutoLogin(mContext, true);
					} else{
						Toast.makeText(mContext, "먼저 로그인을 해주세요", Toast.LENGTH_SHORT).show();
						btnAutoLogin.setChecked(false);
					}
				}
				Log.d(TAG, "AutoLogin : " + SaveVal.getAutoLogin(mContext));
			}
		});
		ivBackBtn = (ImageView) findViewById(R.id.ivSettingBackBtn);
		ivHomeBtn = (ImageView) findViewById(R.id.ivSettingHomeBtn);
		tvLoginText = (TextView) findViewById(R.id.tvSettingLoginText);
		tvVersion = (TextView) findViewById(R.id.tvSettingVerstion);

		//		mFacebook = new Facebook(Val.APP_ID);
		//		mAsyncRunner = new AsyncFacebookRunner(mFacebook);
		mFacebook = Val.FACEBOOK;
		mAsyncRunner = Val.ASYNC_RUNNER;

		if(Val.AUTO_LOGIN){
			SessionStore.restore(mFacebook, mContext);
		}
		SessionEvents.addAuthListener(new SampleAuthListener());
		SessionEvents.addLogoutListener(new SampleLogoutListener());
		btnFacebook.init(this, mFacebook);

		ivBackBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { finish(); }
		});
		ivHomeBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { finish(); }
		});
	}

	public class SampleAuthListener implements AuthListener {

		public void onAuthSucceed() {
			if(Val.REDTABLE_LOGIN) {
				Toast.makeText(mContext, "로그인 되었습니다", Toast.LENGTH_SHORT).show();
			}
			mAsyncRunner.request("me", new SampleRequestListener());
		}

		public void onAuthFail(String error) {
			if(Val.REDTABLE_LOGIN){
				Toast.makeText(mContext, "로그인에 실패하였습니다", Toast.LENGTH_SHORT).show();
			}
		}
	}

	public class SampleLogoutListener implements LogoutListener {
		public void onLogoutBegin() {
			if(Val.REDTABLE_LOGIN){
				Toast.makeText(mContext, "로그아웃 중입니다", Toast.LENGTH_SHORT).show();
			}
		}

		public void onLogoutFinish() {
			if(Val.REDTABLE_LOGIN){
				Toast.makeText(mContext, "로그아웃 되었습니다", Toast.LENGTH_SHORT).show();
			} else{
				Toast.makeText(mContext, "레드테이블 서버에 접속하지못해 로그인에 실패하였습니다", Toast.LENGTH_SHORT).show();
			}
			tvLoginText.setText(getString(R.string.login1));
		}
	}

	public class SampleRequestListener extends BaseRequestListener {
		public void onComplete(final String response, final Object state) {
			try {
				// process the response here: executed in background thread
				Log.d("Facebook-Example", "Response: " + response.toString());
				JSONObject json = Util.parseJson(response);
				final String name = json.getString("name");
				final String email = json.getString("email");


				// then post the processed result back to the UI thread
				// if we do not do this, an runtime exception will be generated
				// e.g. "CalledFromWrongThreadException: Only the original
				// thread that created a view hierarchy can touch its views."
				Setting.this.runOnUiThread(new Runnable() {
					public void run() {
						if(Val.REDTABLE_LOGIN){
							tvLoginText.setText(email + getString(R.string.login2));
						}
					}
				});
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());

			}
		}
	}

	public class SampleUploadListener extends BaseRequestListener {

		public void onComplete(final String response, final Object state) {
			try {
				// process the response here: (executed in background thread)
				Log.d("Facebook-Example", "Response: " + response.toString());
				JSONObject json = Util.parseJson(response);
				final String src = json.getString("src");

				// then post the processed result back to the UI thread
				// if we do not do this, an runtime exception will be generated
				// e.g. "CalledFromWrongThreadException: Only the original
				// thread that created a view hierarchy can touch its views."
				Setting.this.runOnUiThread(new Runnable() {
					public void run() {
						//                        mText.setText("Hello there, photo has been uploaded at \n" + src);
					}
				});
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
		}
	}
	public class WallPostRequestListener extends BaseRequestListener {

		public void onComplete(final String response, final Object state) {
			Log.d("Facebook-Example", "Got response: " + response);
			String message = "<empty>";
			try {
				JSONObject json = Util.parseJson(response);
				message = json.getString("message");
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
			final String text = "Your Wall Post: " + message;
			Setting.this.runOnUiThread(new Runnable() {
				public void run() {
					//                    mText.setText(text);
				}
			});
		}
	}

	public class WallPostDeleteListener extends BaseRequestListener {

		public void onComplete(final String response, final Object state) {
			if (response.equals("true")) {
				Log.d("Facebook-Example", "Successfully deleted wall post");
				Setting.this.runOnUiThread(new Runnable() {
					public void run() {
						//                        mDeleteButton.setVisibility(View.INVISIBLE);
						//                        mText.setText("Deleted Wall Post");
					}
				});
			} else {
				Log.d("Facebook-Example", "Could not delete wall post");
			}
		}
	}

	public class SampleDialogListener extends BaseDialogListener {

		public void onComplete(Bundle values) {
			final String postId = values.getString("post_id");
			if (postId != null) {
				Log.d("Facebook-Example", "Dialog Success! post_id=" + postId);
				mAsyncRunner.request(postId, new WallPostRequestListener());
				//				mDeleteButton.setOnClickListener(new OnClickListener() {
				//					public void onClick(View v) {
				//						mAsyncRunner.request(postId, new Bundle(), "DELETE",
				//								new WallPostDeleteListener(), null);
				//					}
				//				});
				//				mDeleteButton.setVisibility(View.VISIBLE);
			} else {
				Log.d("Facebook-Example", "No wall post made");
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		mFacebook.authorizeCallback(requestCode, resultCode, data);
	}
}