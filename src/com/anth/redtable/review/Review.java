package com.anth.redtable.review;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.anth.redtable.R;
import com.anth.redtable.camera.CameraD;
import com.anth.redtable.function.SimpleDialog;
import com.anth.redtable.function.Val;
import com.anth.redtable.search.SendReviewTask;
import com.anth.redtable.values.Restaurant;

public class Review extends Activity implements OnClickListener {
	private final String TAG = "RedTable_Review";
	private Context mContext;

	private EditText etReview;
	private ArrayList<ImageView> markSets, thumbnails;
	private Intent thumbnailIntent;
	private Intent fromListIntent;
	private RatingBar rbGet, rbSet;

	/** ImageView(Btn) **/
	private ImageView ivRegisterBtn;
	private ImageView ivBackBtn, ivHomeBtn;
	
	/** UI Text **/
	private TextView tvResName;
	private TextView tvResType;
	private TextView tvResRating;
	private TextView tvResAmount;
	private TextView tvSatisfaction;

	/** 리뷰작성 데이터 **/
	private String id;
	private String rest_id;
	private String review;
	private ArrayList<String> photoNameList;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.review);
		mContext = this;
		
		photoNameList = new ArrayList<String>();

		Intent fromListIntent = getIntent();

		//		for(int i=0; i<Val.photoCount; i++){
		//			photoNameList.add("redtabletemp" + i + ".jpg");
		//		}
		tvResName = (TextView) findViewById(R.id.tvReviewResName);
		tvResType = (TextView) findViewById(R.id.tvReviewResType);
		tvResRating = (TextView) findViewById(R.id.tvReviewRating);
		tvResAmount = (TextView) findViewById(R.id.tvReviewAmount);
		tvSatisfaction = (TextView) findViewById(R.id.tvReviewSatisfaction);
		
		/** ImageView(Btn) 설정 **/
		ivBackBtn = (ImageView) findViewById(R.id.ivReviewBackBtn);
		ivHomeBtn = (ImageView) findViewById(R.id.ivReviewHomeBtn);
		ivRegisterBtn = (ImageView) findViewById(R.id.ivReviewRegisterBtn);
		
		ivBackBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { SimpleDialog.makeReviewOutDialog(mContext); }
		});
		ivHomeBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { finish(); }
		});
		ivRegisterBtn.setOnClickListener(this);
		
		
		/** UI Text 삽입 **/
		Restaurant curRes = Val.CUR_RES;
		tvResName.setText(curRes.getName());
		tvResType.setText(curRes.getFood_type());
		tvResRating.setText(curRes.getRti()+"");
		tvResAmount.setText("RTI (" + curRes.getReview_number() +" Reviews)");
		tvSatisfaction.setText("매우 만족");
		

		/** TextView이외 UI설정 **/
		etReview = (EditText) findViewById(R.id.etReview);

		/** RatingBar 설정 
		 * RatingBar의 Max값은 5이므로 모든값은 1/2해주어서 생각한다 */
		rbGet = (RatingBar) findViewById(R.id.rbReviewGet);
		rbGet.setRating((float)curRes.getRti()/2);
		rbGet.setIsIndicator(true);
		
		rbSet = (RatingBar) findViewById(R.id.rbReviewSet);
		rbSet.setRating(5.0f);
		rbSet.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
				if(rating>=0 && rating <=1.0) { tvSatisfaction.setText("매우 불만족"); }
				else if(rating>1.0 && rating <=2.0) { tvSatisfaction.setText("불만족"); }
				else if(rating>2.0 && rating <=3.0) { tvSatisfaction.setText("보통"); }
				else if(rating>3.0 && rating <=4.0) { tvSatisfaction.setText("만족"); }
				else if(rating>4.0 && rating <=5.0) { tvSatisfaction.setText("매우 만족"); }
			}
		});
		
		
		thumbnails = new ArrayList<ImageView>();
		int thumbnailList[] = { R.id.thumbnail1, R.id.thumbnail2, R.id.thumbnail3, R.id.thumbnail4 };
		for(int thumbnailName : thumbnailList){
			ImageView uploadPhoto = (ImageView) findViewById(thumbnailName);
			uploadPhoto.setOnClickListener(this);
			thumbnails.add(uploadPhoto);
		}

		try{
			if(fromListIntent.getStringExtra("from").equals("photoDialog")){
				int number = 0;
				File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/redtabletemp" + 0 + ".jpg");
				int from = Val.PICK_FROM_CAMERA;
				imageProcess(number, file, from);
				Val.photoCount++;
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}



	/** 
	 * onActivity Result
	 * FROM_REVIEW : 사진파일 번호(썸네일 위치와 동일)를 전달받아 파일을 비트맵으로 변환 후  
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK){
			switch(requestCode){
			case Val.FROM_REVIEW:
				int number = data.getIntExtra("no", 5);
				File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/redtabletemp" + number + ".jpg");
				int from = data.getIntExtra("from", 0);
				imageProcess(number, file, from);
			}
		}
	}



//	/** 만족도 표시하는부분. 터치가 드래그될수있게 개선해야함 **/
//	@Override
//	public boolean onTouch(View v, MotionEvent event) {
//		Log.v("Touch!", "X: " + event.getX() + " Y: " + event.getY());
//		if(event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_UP){
//			switch(v.getId()){
//			case R.id.markSet1:
//				markSets.get(0).setImageResource(R.drawable.mark_on);
//				markSets.get(1).setImageResource(R.drawable.mark_off);
//				markSets.get(2).setImageResource(R.drawable.mark_off);
//				markSets.get(3).setImageResource(R.drawable.mark_off);
//				markSets.get(4).setImageResource(R.drawable.mark_off);
//				break;
//			case R.id.markSet2:
//				markSets.get(0).setImageResource(R.drawable.mark_on);
//				markSets.get(1).setImageResource(R.drawable.mark_on);
//				markSets.get(2).setImageResource(R.drawable.mark_off);
//				markSets.get(3).setImageResource(R.drawable.mark_off);
//				markSets.get(4).setImageResource(R.drawable.mark_off);
//				break;
//			case R.id.markSet3:
//				markSets.get(0).setImageResource(R.drawable.mark_on);
//				markSets.get(1).setImageResource(R.drawable.mark_on);
//				markSets.get(2).setImageResource(R.drawable.mark_on);
//				markSets.get(3).setImageResource(R.drawable.mark_off);
//				markSets.get(4).setImageResource(R.drawable.mark_off);
//				break;
//			case R.id.markSet4:
//				markSets.get(0).setImageResource(R.drawable.mark_on);
//				markSets.get(1).setImageResource(R.drawable.mark_on);
//				markSets.get(2).setImageResource(R.drawable.mark_on);
//				markSets.get(3).setImageResource(R.drawable.mark_on);
//				markSets.get(4).setImageResource(R.drawable.mark_off);
//				break;
//			case R.id.markSet5:
//				markSets.get(0).setImageResource(R.drawable.mark_on);
//				markSets.get(1).setImageResource(R.drawable.mark_on);
//				markSets.get(2).setImageResource(R.drawable.mark_on);
//				markSets.get(3).setImageResource(R.drawable.mark_on);
//				markSets.get(4).setImageResource(R.drawable.mark_on);
//				break;
//			}
//		}
//		return true;
//	}

	/** 썸네일부분 터치이벤트 **/
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.thumbnail1:
			thumbnailIntent = new Intent(this, CameraD.class);
			thumbnailIntent.putExtra("no", 0);
			startActivityForResult(thumbnailIntent, Val.FROM_REVIEW);
			break;
		case R.id.thumbnail2:
			thumbnailIntent = new Intent(this, CameraD.class);
			thumbnailIntent.putExtra("no", 1);
			startActivityForResult(thumbnailIntent, Val.FROM_REVIEW);
			break;
		case R.id.thumbnail3:
			thumbnailIntent = new Intent(this, CameraD.class);
			thumbnailIntent.putExtra("no", 2);
			startActivityForResult(thumbnailIntent, Val.FROM_REVIEW);
			break;
		case R.id.thumbnail4:
			thumbnailIntent = new Intent(this, CameraD.class);
			thumbnailIntent.putExtra("no", 3);
			startActivityForResult(thumbnailIntent, Val.FROM_REVIEW);
			break;
		case R.id.ivReviewRegisterBtn:
			for(int i=0; i<Val.photoCount; i++){
				String curPhotoName = "redtabletemp" + i + ".jpg";
				photoNameList.add(curPhotoName);
			}
			review = etReview.getText().toString();
			new SendReviewTask(mContext, photoNameList, review).execute();
			//PostData.sendReview(Val.EMAIL, Val.CUR_RES.getRest_id(), review, photoNameList, mContext);
			break;
		}
	}

	/** Back버튼 클릭시 종료확인 팝업 **/
	@Override
	public void onBackPressed() {
		SimpleDialog.makeReviewOutDialog(mContext);
	}

	private void imageProcess(int number, File file, int from){
		try {
			Bitmap photoBitmap = Media.getBitmap(getContentResolver(), Uri.fromFile(file));
			/** 카메라에서 찍은 사진일경우 revisionBitmapFromCamera메소드 실행 **/
			if(from == Val.PICK_FROM_CAMERA) { photoBitmap = BitmapEdit.revisionBitmapFromCamera(photoBitmap); }
			/** 썸네일용 이미지 편집&적용 */
			Bitmap thumbnail = BitmapEdit.makeResizeBitmap(photoBitmap, Val.RESIZE_HEIGHT);
			int curPosition = number;
			thumbnails.get(curPosition).setImageBitmap(thumbnail);
			/** 업로드용 이미지 편집&파일저장 */
			Bitmap finalResize = BitmapEdit.makeResizeBitmap(photoBitmap, Val.RESIZE_HEIGHT2);
			FileOutputStream fos = new FileOutputStream(file);
			finalResize.compress(CompressFormat.JPEG, 70, fos);
			fos.close();
		} catch (FileNotFoundException e) {
			Log.d(TAG, "Cannot find File - Review");
			e.printStackTrace();
		} catch (IOException e) {
			Log.d(TAG, "find File Error");
			e.printStackTrace();
		}
	}



}