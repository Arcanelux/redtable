package com.anth.redtable.camera;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * SurfaceView를 사용하기 위해서 SurfaceHolder.Callback을 구현하는 클래스 생성
 * SurfaceHolder.Callback을 설정함으로써 Surface가 생성/소멸되었음을 알 수 있음
 * @author lhy
 *
 */
public class CamView extends SurfaceView implements SurfaceHolder.Callback{
	private SurfaceHolder holder;	//실제 Contents표시되는 Surface영역 관리가능
	public Camera camera = null;	//SurfaceView에 Camera에서 받은 영상을 표시하기 위한 Camera객체 추가
	public byte[] photoData;

	public CamView(Context context){
		super(context);
		/**
		 * SurfaceView의 SurfaceHolder Instance를 연결해준다
		 * holder객체를 통해 이 SurfaceView의 Surface에 접근할 수 있게 된다
		 */
		holder = getHolder();

		/**
		 * SurfaceHolder와 SurfaceView를 연결시켜주는 과정
		 * Surface가 변경됨을 SurfaceHolder(holder)를 거쳐 최종적으로 SurfaceView에서도 알 수 있다
		 */
		holder.addCallback(this);

		/** Camera Preview는 별도의 버퍼가 없어도 되므로 위와 같이 옵션을 설정한다 */
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public CamView(Context context, AttributeSet attrs){
		super(context, attrs);
	}
	public CamView(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
	}

	/** SurfaceHolder.Callback에 있는 Interface Methods */
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		camera = Camera.open();
		Log.v("camera", "surfaceCreated");
		try{
			camera.setPreviewDisplay(holder);
			Log.v("camera", "camera open success");
		} catch(Exception e){
			Log.v("camera", "camera open failed");
			camera.release();
			camera = null;
		}
	}

	/** 표시할영역의 크기를 알았으므로 해당 크기로 Preview를 시작 */
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		Camera.Parameters params = camera.getParameters();
		List<Camera.Size> cSize = params.getSupportedPreviewSizes();	//Preview가능한 값을 먼저 리스트에 저장
		Size wideSize = cSize.get(0);
		for(int i=0; i<cSize.size(); i++){
			Size curSize = cSize.get(i);
			if(curSize == null) break;
			int curWidth = curSize.width;
			int curHeight = curSize.height;
			if((curWidth/4)*3 == curHeight) {
				wideSize = curSize;
				break;
			}
		}
		Camera.Size tmpSize = wideSize;								//저장한 값들중에 지정하여 사용

		params.setPreviewSize(tmpSize.width, tmpSize.height);
		camera.setParameters(params);

		camera.setDisplayOrientation(90);
		camera.startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		camera.stopPreview();
		camera.release();
		camera = null;
	}
/*
	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		FileOutputStream fos = null;
		String fileName = "/RedTableCapture.jpg";				

		File sd = Environment.getExternalStorageDirectory();
		File file = new File(sd, fileName);

		try {
			fos = new FileOutputStream(file);
			fos.write(data);
			fos.flush();
			fos.close();				
		} catch (Exception e) {
			Log.e("RedTable", "Photo Save Error");
		}
	}

	public void capture(){
		camera.takePicture(null, null, this);
	}
*/

}