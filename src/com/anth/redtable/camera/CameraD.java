package com.anth.redtable.camera;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.anth.redtable.R;
import com.anth.redtable.R.id;
import com.anth.redtable.R.layout;
import com.anth.redtable.function.Val;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

public class CameraD extends Activity implements OnClickListener{
	private static final String TAG = "Camera";
	private Uri mImageCaptureUri;

	private Uri capturedImageUri, croppedImageUri;
	public CamView camView = null;
	public Bitmap cropped = null;
	public byte[] photoData = null;

	static final int CAMERA = 1001;
	static final int ALBUM = 1001;
	static final int CHOOSER = 1001;
	static final int CROP = 1001;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.camera);

		/** FrameLayout에 SurfaceView(camView)를 이용해 카메라 프리뷰 설정 */
		FrameLayout cameraFrame = (FrameLayout) findViewById(R.id.cameraFrame);
		camView = new CamView(this);
		cameraFrame.addView(camView);

		/** 각 버튼들 선언 */
		ImageView capture = (ImageView) findViewById(R.id.capture);
		ImageView album = (ImageView) findViewById(R.id.album);
		ImageView photo_back = (ImageView) findViewById(R.id.photo_back);
		ImageView brightness = (ImageView) findViewById(R.id.brightness);
		ImageView rotate = (ImageView) findViewById(R.id.rotate);

		capture.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				camView.camera.takePicture(null, null, mPictureListener);
			}
		});
		album.setOnClickListener(this);

		//cropedImageUri = null;
	}

	/** onActivityResults **/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		Log.d(TAG, "onActivityResult");
		if(resultCode != RESULT_OK)	{
			return;
		}

		switch(requestCode)	{
		case Val.PICK_FROM_ALBUM:{
			Log.d(TAG, "PICK_FROM_ALBUM");

			// 이후의 처리가 카메라와 같으므로 일단  break없이 진행합니다.
			// 실제 코드에서는 좀더 합리적인 방법을 선택하시기 바랍니다.
			mImageCaptureUri = data.getData();
			File original_file = getImageFile(mImageCaptureUri);

			mImageCaptureUri = createSaveCropFile();
			File copy_file = new File(mImageCaptureUri.getPath()); 

			// SD카드에 저장된 파일을 이미지 Crop을 위해 복사한다.
			copyFile(original_file , copy_file);
		}
		case Val.PICK_FROM_CAMERA:{
			Log.d(TAG, "PICK_FROM_CAMERA"); 

			// 이미지를 가져온 이후의 리사이즈할 이미지 크기를 결정합니다.
			// 이후에 이미지 크롭 어플리케이션을 호출하게 됩니다.

			Intent intent = new Intent("com.android.camera.action.CROP");
			intent.setDataAndType(mImageCaptureUri, "image/*"); 

			// Crop한 이미지를 저장할 Path
			Intent captured = getIntent();
			String url = "/redtabletemp" + captured.getIntExtra("no", 5) + ".jpg";
			mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url)); 
			intent.putExtra("output", mImageCaptureUri);
			Log.d(TAG, "Output :" + mImageCaptureUri);

			// Return Data를 사용하면 번들 용량 제한으로 크기가 큰 이미지는
			// 넘겨 줄 수 없다.
			//          intent.putExtra("return-data", true); 
			startActivityForResult(intent, Val.CROP_FROM);
			break;
		}

		case Val.CROP_FROM:{
			Log.w(TAG, "CROP_FROM_CAMERA");
			// Crop 된 이미지를 넘겨 받습니다.
			Log.w(TAG, "mImageCaptureUri = " + mImageCaptureUri);

			String full_path = mImageCaptureUri.getPath();
			String photo_path = full_path.substring(4, full_path.length());

			Log.w(TAG, "비트맵 Image path = "+photo_path);

			//Bitmap photo = BitmapFactory.decodeFile(photo_path);
			//mPhotoImageView.setImageBitmap(photo);
			Intent captured = getIntent();
			captured.putExtra("no", getIntent().getIntExtra("no", 5));
			captured.putExtra("from", Val.CROP_FROM);
			setResult(RESULT_OK, captured);

			onBackPressed();
			break;
		}
		}	//switch
	}	//onActivityResult

	/**
	 * Crop된 이미지가 저장될 파일을 만든다.
	 * @return Uri
	 */
	private Uri createSaveCropFile(){
		Uri uri;
		String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
		uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));
		return uri;
	}


	/**
	 * 선택된 uri의 사진 Path를 가져온다.
	 * uri 가 null 경우 마지막에 저장된 사진을 가져온다.
	 * @param uri
	 * @return
	 */
	private File getImageFile(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		if (uri == null) {
			uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		}

		Cursor mCursor = getContentResolver().query(uri, projection, null, null, 
				MediaStore.Images.Media.DATE_MODIFIED + " desc");
		if(mCursor == null || mCursor.getCount() < 1) {
			return null; // no cursor or no record
		}
		int column_index = mCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		mCursor.moveToFirst();

		String path = mCursor.getString(column_index);

		if (mCursor !=null ) {
			mCursor.close();
			mCursor = null;
		}

		return new File(path);
	}

	/**
	 * 파일 복사
	 * @param srcFile : 복사할 File
	 * @param destFile : 복사될 File
	 * @return
	 */
	public static boolean copyFile(File srcFile, File destFile) {
		boolean result = false;
		try {
			InputStream in = new FileInputStream(srcFile);
			try {
				result = copyToFile(in, destFile);
			} finally  {
				in.close();
			}
		} catch (IOException e) {
			result = false;
		}
		return result;
	}

	/**
	 * Copy data from a source stream to destFile.
	 * Return true if succeed, return false if failed.
	 */
	private static boolean copyToFile(InputStream inputStream, File destFile) {
		try {
			OutputStream out = new FileOutputStream(destFile);
			try {
				byte[] buffer = new byte[4096];
				int bytesRead;
				while ((bytesRead = inputStream.read(buffer)) >= 0) {
					out.write(buffer, 0, bytesRead);
				}
			} finally {
				out.close();
			}
			return true;
		} catch (IOException e) {
			return false;
		}
	}


	/** 앨범 호출 하기 */
	private void doTakeAlbumAction(){
		// 앨범 호출
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
		startActivityForResult(intent, Val.PICK_FROM_ALBUM);
	}


	private Camera.PictureCallback mPictureListener = new Camera.PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera){
			/** 리사이즈부분 - 봉인 */
			/*
			int resizeHeight = 200;
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, null);
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();
			Bitmap resized = null;
			while(height > resizeHeight){
				resized = Bitmap.createScaledBitmap(bitmap, width*resizeHeight/height, resizeHeight, true);
				width = resized.getWidth();
				height = resized.getHeight();
			}
			int cw = width - resizeHeight;
			int cy = 0;
			cropped = Bitmap.createBitmap(resized, cw, cy, resizeHeight, resizeHeight);

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			cropped.compress(CompressFormat.JPEG, 80, bos);

			photoData = bos.toByteArray();
			 */
			Intent capturedIntent = getIntent();

			// photoCount-1(사진개수 -1/1,2,3,4)보다 썸네일넘버(0,1,2,3)가 더 클 경우
			// 즉, 사진개수보다 더 오른쪽 칸을 눌러 사진을 찍을경우, 현재까지 삽입된 사진바로 다음으로 파일이름을 지정
			FileOutputStream fos = null;
			int number = capturedIntent.getIntExtra("no", 5);
			if(Val.photoCount-1 < number && number!=5){
				number = Val.photoCount;
			}
			String fileName = "/redtabletemp" + number + ".jpg";

			File sd = Environment.getExternalStorageDirectory();
			File file = new File(sd, fileName);
			try {
				fos = new FileOutputStream(file);
				fos.write(data);
				fos.flush();
				fos.close();
				if(Val.photoCount-1 < number){ Val.photoCount++; }
				Log.d(TAG, "SavePhoto : " + fileName + "\nphotoCount : " + Val.photoCount);
				Toast.makeText(getApplicationContext(), "SavePhoto : " + fileName + "\nphotoCount : " + Val.photoCount, Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), "PhotoSave Failed", Toast.LENGTH_SHORT).show();
			}

			// Review로 파일명과 입력된 칸을 나타내는 int형 number값을 전달
			capturedIntent.putExtra("no", number);
			capturedIntent.putExtra("from", Val.PICK_FROM_CAMERA);
			setResult(RESULT_OK, capturedIntent);
			onBackPressed();
		}
	};

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.album:
			Toast.makeText(getApplicationContext(), "album Clicked", Toast.LENGTH_SHORT).show();
			doTakeAlbumAction();
			break;
		}

	}
}