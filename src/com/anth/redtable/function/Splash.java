package com.anth.redtable.function;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.anth.redtable.R;
import com.anth.redtable.facebook.BaseRequestListener;
import com.anth.redtable.facebook.SessionStore;
import com.anth.redtable.facebook.LoginButton.SampleRequestListener;
import com.anth.redtable.main.RedTableActivity;
import com.anth.redtable.setting.Setting;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class Splash extends Activity{
	private Context mContext;

	private ProgressDialog mProgressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN); 
		setContentView(R.layout.splash);
		mContext = this;

		new FacebookLoginTask().execute();
	}	

	class FacebookLoginTask extends AsyncTask<Void, String, Integer>{
		@Override
		protected void onPreExecute() {
			//			mProgressDialog = new ProgressDialog(mContext);
			//			mProgressDialog.setMessage("Initializing...");
			//			mProgressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(Void... unused) {
			DeleteTemp.deletePhoto();
			if(Val.AUTO_LOGIN){
				Facebook mFacebook = Val.FACEBOOK;
				SessionStore.restore(mFacebook, mContext);
				if(mFacebook.isSessionValid()){
					Val.ASYNC_RUNNER.request("me", new EmailRequestListener());
				}
			}
			return 0;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onCancelled() {
			//			mProgressDialog.dismiss();
			finish();
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(Integer result) {
			//			mProgressDialog.dismiss();
			startActivity(new Intent(Splash.this, RedTableActivity.class));
			finish();
			super.onPostExecute(result);
		}

	}

	public class EmailRequestListener extends BaseRequestListener {
		public void onComplete(final String response, final Object state) {
			try {
				// process the response here: executed in background thread
				Log.d("Facebook-Example", "Response: " + response.toString());
				JSONObject json = Util.parseJson(response);
				//				final String name = json.getString("name");
				final String email = json.getString("email");

				Val.EMAIL = email;
				// then post the processed result back to the UI thread
				// if we do not do this, an runtime exception will be generated
				// e.g. "CalledFromWrongThreadException: Only the original
				// thread that created a view hierarchy can touch its views."
				//				Splash.this.runOnUiThread(new Runnable() {
				//					public void run() {
				//						//                        mText.setText("Hello there, " + name + "!");
				////						Toast.makeText(mContext, "name:" + name, Toast.LENGTH_SHORT).show();
				////						tvLoginText.setText(email + getString(R.string.login2));
				//					}
				//				});
			} catch (JSONException e) {
				Log.w("Facebook-Example", "JSON Error in response");
			} catch (FacebookError e) {
				Log.w("Facebook-Example", "Facebook Error: " + e.getMessage());
			}
		}
	}
}
