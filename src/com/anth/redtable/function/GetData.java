package com.anth.redtable.function;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.anth.redtable.values.Restaurant;

public class GetData {
	private static final String TAG = "RedTable_GetData";

	/** 레스토랑 리스트 생성 또는 10개씩 추가해주는 함수 **/
	public static ArrayList<Restaurant> makeResList(Context context, ArrayList<Restaurant> mResList, String mArea_code1, String mArea_code2, Integer mPage, Integer mLimit, boolean mMode){
		if(mMode == Val.makeMode) { mResList = new ArrayList<Restaurant>(); }

		String apiUrl = Val.API_LIST;
		String area_code1 = mArea_code1;
		String area_code2 = mArea_code2;
		Integer page = mPage;
		Integer limit = mLimit;

		String curUrl = apiUrl + "?";

		if(!area_code1.equals(""))	curUrl += "area_code1=" + area_code1 + "&";
		if(!area_code2.equals("")) 	curUrl += "area_code2=" + area_code2 + "&";
		if(page != null)			curUrl += "page=" + page + "&";
		if(limit != null)			curUrl += "limit=" + limit + "&";

		JSONObject curJson = JsonFunc.getJSONfromURL(curUrl);
		JSONArray results = null;
		int resultSize = 0;
		try {
			results = curJson.getJSONArray("results");
			Restaurant aRestaurant = null;
			// 길이 판단용
			resultSize = results.length();
			for(int i=0; i<results.length(); i++){
				JSONObject curRes = results.getJSONObject(i);

				String rest_id = curRes.getString("rest_id");
				String name = curRes.getString("name");
				String address = curRes.getString("address");
				double rti = curRes.getDouble("rti");
				String food_type = curRes.getString("food_type");
				int review_number = curRes.getInt("review_number");
				String detail_link = curRes.getString("detail_link");
				double latitude = curRes.getDouble("latitude");
				double longitude = curRes.getDouble("longitude");

				aRestaurant = new Restaurant(rest_id, name, address, rti, food_type, review_number, detail_link, latitude, longitude);

				mResList.add(aRestaurant);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if(resultSize==0){
			Toast.makeText(context, "리스트의 끝입니다", Toast.LENGTH_SHORT).show();
		}
		//		mResList.add(null);	//더보기 버튼을 위한 더미

		return mResList;
	}

	public static ArrayList<Restaurant> makeResSearchList(Context context, ArrayList<Restaurant> mResList, Integer mPage, Integer mLimit, boolean mMode, String keyword){
		if(mMode == Val.makeMode) { mResList = new ArrayList<Restaurant>(); }
		Log.d(TAG, "SearchKeyword : " + keyword);
		String encodedKeyword = null;
		try {
			encodedKeyword = URLEncoder.encode(keyword, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			Log.d(TAG, "keyword encoding Failed");
			e1.printStackTrace();
		}
		Log.d(TAG, "EncodedSearchKeyword : " + encodedKeyword);
		String searchUrl = Val.API_SEARCH + "?" + "q=" + encodedKeyword
				+ "&page=" + mPage + "&limit=" + mLimit;

		Log.d(TAG, "Search Get Url : " + searchUrl);

		int resultSize = 0;
		JSONObject resultJSON = JsonFunc.getJSONfromURL(searchUrl);
		JSONArray results = null;
		try {
			results = resultJSON.getJSONArray("results");
			Restaurant aRestaurant = null;
			// 길이 판단(없을경우 토스트 출력)
			resultSize = results.length();
			for(int i=0; i<results.length(); i++){
				JSONObject curRes = results.getJSONObject(i);
				
				int review_number = curRes.getInt("review_number");
				String address = curRes.getString("address");
				String name = curRes.getString("name");
				String food_type = curRes.getString("food_type");
				double longitude = curRes.getDouble("longitude");
				double latitude = curRes.getDouble("latitude");
				String detail_link = curRes.getString("detail_link");
				String rest_id = curRes.getString("rest_id");
				double rti = curRes.getDouble("rti");
				
				aRestaurant = new Restaurant(rest_id, name, address, rti, food_type, review_number, detail_link, latitude, longitude);
				mResList.add(aRestaurant);
			}
		}  catch(JSONException e){
			e.printStackTrace();
		}
		if(resultSize==0){
			Toast.makeText(context, "리스트의 끝입니다", Toast.LENGTH_SHORT).show();
		}
		return mResList;
	}
}