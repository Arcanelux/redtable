package com.anth.redtable.function;

import com.anth.redtable.values.Region;
import com.anth.redtable.values.Restaurant;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;

public class Val {
	/** Facebook **/
	public static final String APP_ID = "393057907430906";
	public static Facebook FACEBOOK = new Facebook(APP_ID);
	public static AsyncFacebookRunner ASYNC_RUNNER = new AsyncFacebookRunner(FACEBOOK);
	
	public static String EMAIL = "";
	
	/**
	 * 어느부분에서 호출했는지 판단위한 int변수
	 */
	public static final int NONE = 0;
	
	public static final int FROM_MAIN = 100;
	public static final int FROM_SEARCH = 101;
	public static final int FROM_REVIEW = 102;
	
	public static final int PICK_FROM_ALBUM = 110;
	public static final int PICK_FROM_CAMERA = 111;
	public static final int CROP_FROM = 115;
	
	public static final int TO_CHECKIN = 150;
	public static final int TO_CAMERA = 151;
	public static final int TO_REVIEW = 152;
	public static final int TO_SCRAP = 153;
	public static final int TO_SETTING = 154;
	
	public static int screenWidth;
	public static int screenHeight;
	
	/** 레스토랑 리스트 생성시 **/
	public static final boolean addMode = false;
	public static final boolean makeMode = true;
	
	private static final String API = "http://203.250.148.10/api/";
	public static final String API_LIST = API + "list.json";
	public static final String API_WRITE = API + "write.json";
	public static final String API_JOIN = API + "join.json";
	public static final String API_MYPAGE = API + "mypage.json";
	public static final String API_SEARCH = API + "search.json";
	public static final String API_LIST_RADIUS = API + "list_radius.json";
	
	/** Review에 삽입된 사진 개수 **/
	public static int photoCount = 0;
	
	/** 사진 리사이즈 크기 **/
	public static int RESIZE_HEIGHT = 200;		// 썸네일
	public static int RESIZE_HEIGHT2 = 400;	// 전송할 파일
	
	/** 현재 선택된 레스토랑 **/
	public static Restaurant CUR_RES;
	
	/** 현재 선택된 지역 **/
	public static Region CUR_REGION;
	
	/** 레드테이블 로그인 성공여부 **/
	public static boolean REDTABLE_LOGIN = true;
	
	/** 자동로그인 여부 **/
	public static boolean AUTO_LOGIN = false;
}
