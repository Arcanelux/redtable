/**
 * Author : Lee HanYeong
 * File Name : SaveVal.java
 * Created Date : 2012. 9. 28.
 * Description
 */
package com.anth.redtable.function;

import android.content.Context;
import android.content.SharedPreferences;

public class SaveVal {
	public static void setAutoLogin(Context context, boolean autoLogin){
		SharedPreferences pref = context.getSharedPreferences("AutoLogin", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("AutoLogin", autoLogin);
        editor.commit();
	}
	public static boolean getAutoLogin(Context context){
		SharedPreferences pref = context.getSharedPreferences("AutoLogin", context.MODE_PRIVATE);
		return pref.getBoolean("AutoLogin", true);
	}
}
