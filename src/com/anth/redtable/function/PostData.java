package com.anth.redtable.function;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.anth.redtable.values.Restaurant;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ParseException;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.util.Log;

public class PostData {
	public static final String TAG = "RedTable_PostData";
	public static final String TAG2 = "PostData";
	public static boolean SEND_SUCCESS = false;

	public static void sendCheckin(String id, String rest_id, Context context){
		SEND_SUCCESS = false;
		ArrayList<String> photoNameList = new ArrayList<String>();
		sendReview(id, rest_id, "", photoNameList, context);
	}

	public static void sendPhoto(String id, String rest_id, ArrayList<String> photoNameList, Context context){
		SEND_SUCCESS = false;
		sendReview(id, rest_id, "", photoNameList, context);
	}
	public static void sendReview(String id, String rest_id, String review, ArrayList<String> photoNameList, Context context) {
		SEND_SUCCESS = false;
		Log.d(TAG, "-- Start sendReview");
		if(review.equals("") && photoNameList==null){
			Log.d(TAG2, "--Mode : Checkin");
		} else if(review.equals("")){
			Log.d(TAG2, "--Mode : Photo");
		} else{
			Log.d(TAG2, "--Mode : Review");
		}

		Log.d(TAG2, "-Data writing Start");
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "P3FS4PKIS";      // 임의로 설정한다

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		// 전송할 데이터
		ArrayList<Bitmap> photoList = new ArrayList<Bitmap>();
		ArrayList<byte[]> byteImageList = new ArrayList<byte[]>();

		Log.d(TAG2, "-PhotoData write...");
		for(String curPhotoName : photoNameList){
			File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + curPhotoName);
			Bitmap curBitmap;
			Log.d(TAG2, "Bitmap to Byte : " + curPhotoName);
			try {
				curBitmap = Media.getBitmap(context.getContentResolver(), Uri.fromFile(file));
				ByteArrayOutputStream  byteArray = new ByteArrayOutputStream();
				curBitmap.compress(CompressFormat.JPEG, 50, byteArray);

				byte[] byteImage= null;
				byteImage = byteArray.toByteArray();
				byteImageList.add(byteImage);
			} 
			catch (FileNotFoundException e) { 
				e.printStackTrace(); 
				Log.d(TAG2, "File \"" + curPhotoName + "\" to byteArray Failed (FileNotFoundException)");
				return;
			}
			catch (IOException e) {
				e.printStackTrace();
				Log.d(TAG2, "File \"" + curPhotoName + "\" to byteArray Failed (IOException)");
				return;
			}
		}

		// DataOutputStream에 데이터 삽입
		// 전송데이터가 null일경우 Exception이 발생함

		for(int i=0; i<byteImageList.size(); i++){
			// 이미지 파일 등 byte[] 형태의 전송
			// DataOutputStream에 twoHyphens + boundary + lineEnd 로 각 데이터의 시작부분을 알려준다
			try{
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				String imageName = "photo" + i + ".jpg";
				Log.d(TAG2, "Byte to DataOutputStream : " + imageName);

				dos.writeBytes("Content-Disposition: form-data; filename=\"" + imageName + "\"; name=\"ImageParameter\"" + lineEnd);
				dos.writeBytes("Content-Type: image/pjpeg " + lineEnd);

				dos.writeBytes(lineEnd);
				dos.write(byteImageList.get(i), 0, byteImageList.get(i).length);
				dos.writeBytes(lineEnd);
			} catch(Exception e){
				e.printStackTrace();
				Log.d(TAG2, "ImageByteData write to DataOutputStream Failed");
				return;
			}
		}
		Log.d(TAG2, "-PhotoData write Complete");

		Log.d(TAG2, "-StringData write...");
		try{
			// 필수 데이터(id, rest_id, checkin)
			Log.d(TAG2, "id, rest_id, checkin write...");
			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"id\"" + lineEnd);
			dos.writeBytes(lineEnd);
			dos.writeBytes(id + lineEnd);

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"rest_id\"" + lineEnd);
			dos.writeBytes(lineEnd);
			dos.writeBytes(rest_id + lineEnd);

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"checkin\"" + lineEnd);
			dos.writeBytes(lineEnd);
			dos.writeBytes(1 + lineEnd);
			Log.d(TAG2, "id, rest_id, checkin write Complete");

			Log.d(TAG2, "review write...");
			// 리뷰는 선택적 전송
			if(!review.equals("")){
				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"review\"" + lineEnd);
				dos.writeBytes(lineEnd);
				dos.writeBytes(review + lineEnd);
			}
			Log.d(TAG2, "review write Complete");

			// 전송 데이터 끝 표시
			// DataOutputStream에 twoHyphens + boundary + twoHyphens + lineEnd 로 끝을 알려준다
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			dos.flush();
			dos.close();
			Log.d(TAG2, "-Data writing End");

		} catch(Exception e){
			e.printStackTrace();
			Log.d(TAG2, "StringData write to DataOutputStream Failed");
			return;
		}

		Log.d(TAG2, "-Data to HttpEntity Start");
		// DataOutputStream으로부터 ByteArrayOutputStream으로 전달된 데이터를 이용하여
		// ByteArrayInputStream객체를 생성, BasicHttpEntity에 전달해준다
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		BasicHttpEntity entity = new BasicHttpEntity();
		entity.setContent(bais);
		Log.d(TAG2, "-Data to HttpEntity End");

		Log.d(TAG2, "-HttpPost Start");
		// HttpPost 설정
		// "Content-Type"을 "multipart/form-data; boundary=미리 정의한 임의의 boundary" 로 설정한다
		HttpPost httpPost = new HttpPost(Val.API_WRITE);
		httpPost.addHeader("Connection", "Keep-Alive");
		httpPost.addHeader("Content-Type", "multipart/form-data; boundary="+boundary);
		httpPost.setEntity(entity);

		// HttpClient 할당, 세션 유지를 위해 HttpClient의 재사용을 원한다면
		// 외부 클래스에 static으로 선언하여 전송함수에서 참조한다
		// ex) Val.java파일 - public static HttpClient httpClient = new DefaultHttpClient();
		// --- 함수내부 -       HttpClient httpClient = Val.httpClient;
		HttpClient httpClient = new DefaultHttpClient();

		// 전송, HttpResponse에 결과값 할당
		HttpResponse response = null;
		try {
			response = httpClient.execute(httpPost);
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
			return;
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}
		Log.d(TAG2, "-HttpPost End");


		Log.d(TAG2, "-Result Process Start");
		// 통신결과값 처리
		String result = null;
		HttpEntity httpEntity = response.getEntity();
		JSONObject resultJSON = null;
		try {
			String resultJsonString = EntityUtils.toString(httpEntity);
			resultJSON = new JSONObject(resultJsonString);
			try{
				result = resultJSON.getString("result");
			} catch(Exception e){
				e.printStackTrace();
				Log.d(TAG2, "get resultJSON result Failed");
				return;
			}
		}  
		catch (ParseException e) { e.printStackTrace(); return; }
		catch (IOException e) { e.printStackTrace(); return; }
		catch (JSONException e) { e.printStackTrace(); return; }
		Log.d(TAG2, "-Result Process End");

		//		Toast.makeText(context, "sendReview Result : " + result, Toast.LENGTH_SHORT).show();
		Log.d(TAG, "-sendReview Result: " + result);
		if(result.equals("success")){
			Log.d(TAG, "-SEND_SUCCESS = true");
		}
		SEND_SUCCESS = true;
		Log.d(TAG, "--End sendReview");
	}

	public static boolean join(){
		String joinUrl = Val.API_JOIN;
		try {
			// URL설정, 접속
			URL url = new URL(joinUrl);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();

			// 전송모드 설정(일반적인 POST방식)
			http.setDefaultUseCaches(false);
			http.setDoInput(true);
			http.setDoOutput(true);
			http.setRequestMethod("POST");

			// content-type 설정
			http.setRequestProperty("Content-type", "application/x-www-form-urlencoded");

			// 전송값 설정
			StringBuffer buffer = new StringBuffer();
			buffer.append("id").append("=").append(Val.EMAIL);
			
			// 서버로 전송
			OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "UTF-8");
			PrintWriter writer = new PrintWriter(outStream);
			writer.write(buffer.toString());
			writer.flush();

			// 전송 결과값 받기
			InputStreamReader inputStream = new InputStreamReader(http.getInputStream(), "UTF-8");
			BufferedReader bufferReader = new BufferedReader(inputStream);
			StringBuilder builder = new StringBuilder();
			String str;
			while((str = bufferReader.readLine()) != null){
				builder.append(str + "\n");
			}
			String result = null;
			String resultJSONString = builder.toString();
			JSONObject resultJSON = null;
			try {
				resultJSON = new JSONObject(resultJSONString);
				try{
					result = resultJSON.getString("result");
				} catch(Exception e){
					e.printStackTrace();
					Log.d(TAG, "get resultJSON result Failed");
					return false;
				}
			}  
			catch (ParseException e) { e.printStackTrace(); return false; }
			catch (JSONException e) { e.printStackTrace(); return false; }
			Log.d(TAG, "전송결과 : " + result);
			
			if(result.equals("success") || result.equals("id is already inserted")){
				return true;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	public static String getMyPageUrl(){
		String myPageUrl = Val.API_MYPAGE;
		try {
			// URL설정, 접속
			URL url = new URL(myPageUrl);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();

			// 전송모드 설정(일반적인 POST방식)
			http.setDefaultUseCaches(false);
			http.setDoInput(true);
			http.setDoOutput(true);
			http.setRequestMethod("POST");

			// content-type 설정
			http.setRequestProperty("Content-type", "application/x-www-form-urlencoded");

			// 전송값 설정
			StringBuffer buffer = new StringBuffer();
			buffer.append("id").append("=").append(Val.EMAIL);
			
			// 서버로 전송
			OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "UTF-8");
			PrintWriter writer = new PrintWriter(outStream);
			writer.write(buffer.toString());
			writer.flush();

			// 전송 결과값 받기
			InputStreamReader inputStream = new InputStreamReader(http.getInputStream(), "UTF-8");
			BufferedReader bufferReader = new BufferedReader(inputStream);
			StringBuilder builder = new StringBuilder();
			String str;
			while((str = bufferReader.readLine()) != null){
				builder.append(str + "\n");
			}
			String result = null;
			String resultUrl = null;
			String resultJSONString = builder.toString();
			JSONObject resultJSON = null;
			try {
				resultJSON = new JSONObject(resultJSONString);
				try{
//					result = resultJSON.getString("result");
					resultUrl = resultJSON.getString("link");
				} catch(Exception e){
					e.printStackTrace();
					Log.d(TAG, "get resultJSON result Failed");
					return "";
				}
			}  
			catch (ParseException e) { e.printStackTrace(); return ""; }
			catch (JSONException e) { e.printStackTrace(); return ""; }
			Log.d(TAG, "getMyPage Result : " + resultUrl);
			return resultUrl;

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	
	
	

	public void httpPostNormal(){
		try {
			// URL설정, 접속
			URL url = new URL("http://URL");
			HttpURLConnection http = (HttpURLConnection) url.openConnection();

			// 전송모드 설정(일반적인 POST방식)
			http.setDefaultUseCaches(false);
			http.setDoInput(true);
			http.setDoOutput(true);
			http.setRequestMethod("POST");

			// content-type 설정
			http.setRequestProperty("Content-type", "application/x-www-form-urlencoded");

			// 전송값 설정
			StringBuffer buffer = new StringBuffer();
			buffer.append("name").append("=").append("value").append("&");
			buffer.append("name2").append("=").append("value2").append("&");
			buffer.append("name3").append("=").append("value3");

			// 서버로 전송
			OutputStreamWriter outStream = new OutputStreamWriter(http.getOutputStream(), "UTF-8");
			PrintWriter writer = new PrintWriter(outStream);
			writer.write(buffer.toString());
			writer.flush();

			// 전송 결과값 받기
			InputStreamReader inputStream = new InputStreamReader(http.getInputStream(), "UTF-8");
			BufferedReader bufferReader = new BufferedReader(inputStream);
			StringBuilder builder = new StringBuilder();
			String str;
			while((str = bufferReader.readLine()) != null){
				builder.append(str + "\n");
			}
			String result = builder.toString();
			Log.d("HttpPost", "전송결과 : " + result);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}