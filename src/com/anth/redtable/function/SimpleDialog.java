/**
 * Author : Lee HanYeong
 * File Name : SimpleAlertDialog.java
 * Created Date : 2012. 9. 28.
 * Description
 */
package com.anth.redtable.function;

import com.anth.redtable.setting.Setting;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface;

public class SimpleDialog {
	public static void makeNoneResDialog(Context context){
		new AlertDialog.Builder(context)
		.setMessage( "레스토랑 정보가 없습니다" )
		.setPositiveButton( "확인", new DialogInterface.OnClickListener()	{
			@Override
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		})
		.show();
	}

	public static void makeRequireLoginDialog(final Context context){
		new AlertDialog.Builder(context)
		.setMessage( "로그인하셔야 사용가능합니다.\n확인을 누르시면 로그인설정으로 이동합니다" )
		.setPositiveButton( "확인", new DialogInterface.OnClickListener()	{
			@Override
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
//				Intent intent = ((Activity)context), Setting.class);
				Intent intent = new Intent((Activity)context, Setting.class);
				context.startActivity(intent);
			}
		})
		.show();
	}

	public static void makeReviewOutDialog(final Context context){
		new AlertDialog.Builder(context)
		.setMessage( "리뷰작성을 종료하고 나가시겠습니까?" )
		.setPositiveButton( "예", new DialogInterface.OnClickListener()	{
			@Override
			public void onClick(DialogInterface dialog, int which){
				Val.photoCount = 0;
				((Activity)context).finish();
				dialog.dismiss();
			}
		})
		.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		})
		.show();
	}
}
