package com.anth.redtable;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.anth.redtable.function.GetData;
import com.anth.redtable.function.SimpleDialog;
import com.anth.redtable.function.Val;
import com.anth.redtable.search.Search;
import com.anth.redtable.values.Restaurant;
import com.anth.redtable.values.RestaurantArrayAdapter;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class CategoryResult extends MapActivity implements OnClickListener, OnItemClickListener{
	private final String TAG = "RedTable_CategoryResult";
	public static final boolean addMode = false;
	public static final boolean makeMode = true;

	private Context mContext;

	private ImageView ivBackBtn, ivHomeBtn, ivSearchBtn;
	private ArrayList<Restaurant> categoryResultList;
	private ListView categoryResultListView;
	private RestaurantArrayAdapter categoryResultAA;

	private MapView myMap;
	MyLocationOverlay2 mLocation;
	boolean myLocation = true;

	String area_code1;
	String area_code2;
	Integer page = 1;
	Integer limit = 10;
	
	/** MapOverlay **/
	private List<Overlay> mapOverlays;
	private Drawable drawableMarker;
	private MyItemizedOverlay mItemizedOverlay;

	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.category_result);
		mContext = this;

		/** ImageView(Btn) 설정 **/
		ivBackBtn = (ImageView) findViewById(R.id.ivCategoryResultBackBtn);
		ivHomeBtn = (ImageView) findViewById(R.id.ivCategoryResultHomeBtn);
		ivSearchBtn = (ImageView) findViewById(R.id.ivCategoryResultSearchBtn);

		ivBackBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { finish(); }
		});
		ivHomeBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { finish(); }
		});
		ivSearchBtn.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) { 
				finish(); 
			}
		});

		/** MapView 설정 **/
		myMap = (MapView) findViewById(R.id.mapView);
		final MapController mc = myMap.getController();

		myMap.setBuiltInZoomControls(false);
		myMap.setSatellite(false);
		//myMap.setStreetView(true);

		//mapOverlays = myMap.getOverlays();
		//final GeoPoint startGeo = new GeoPoint(37551458, 126925789); //좌표
		//mc.animateTo(startGeo);
		mc.setZoom(18);

		mLocation = new MyLocationOverlay2(this, myMap);
		List<Overlay> overlays = myMap.getOverlays();
		overlays.add(mLocation);
		mLocation.enableMyLocation();

		mLocation.runOnFirstFix(new Runnable(){
			public void run(){
				if(myLocation) myMap.getController().animateTo(mLocation.getMyLocation());
			}
		});


		/** EditText 설정 - 처음엔 키보드가 나오지않으며, 클릭시 키보드 나오게 함 */
		EditText categoryResultEditText = (EditText) findViewById(R.id.categoryResultEditText);
		categoryResultEditText.setInputType(0);
		categoryResultEditText.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				EditText categoryResultEditText = (EditText) findViewById(R.id.categoryResultEditText);
				categoryResultEditText.setInputType(1);
			}
		});

		/** 레스토랑 리스트 생성(JSON) **/
		area_code1 = Val.CUR_REGION.getRegionCode1();
		area_code2 = Val.CUR_REGION.getRegionCode2();

		categoryResultList = new ArrayList<Restaurant>();
		categoryResultList = GetData.makeResList(mContext, categoryResultList, area_code1, area_code2, page, limit, makeMode);

		categoryResultListView = (ListView) findViewById(R.id.categoryResultListView);
		categoryResultAA = new RestaurantArrayAdapter(this, R.layout.restaurant_cell, categoryResultList);

		if(categoryResultList.size()==0){
			SimpleDialog.makeNoneResDialog(mContext);
		} else{
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View footer = inflater.inflate(R.layout.footer, null);
			footer.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.d(TAG, "Footer");
					GetData.makeResList(mContext, categoryResultList, area_code1, area_code2, ++page, limit, Val.addMode);
					categoryResultAA.notifyDataSetChanged();
				}
			});
			categoryResultListView.addFooterView(footer);
		}
		categoryResultListView.setAdapter(categoryResultAA);
		categoryResultListView.setFadingEdgeLength(0);
		categoryResultListView.setOnItemClickListener(this);
		
		/** MapOverlay **/
		mapOverlays = myMap.getOverlays();
		drawableMarker = this.getResources().getDrawable(R.drawable.marker);
		mItemizedOverlay = new MyItemizedOverlay(drawableMarker, this);
		
		for(int i=0; i<categoryResultList.size(); i++){
			Restaurant curRes = categoryResultList.get(i);
			int curLatitude = (int)(curRes.getLatitude() * 1000000);
			int curLongitude = (int)(curRes.getLongitude() * 1000000);
			OverlayItem curOverlayitem = new OverlayItem(new GeoPoint(curLatitude, curLongitude), curRes.getName(), curRes.getName());
			
			mItemizedOverlay.addOverlay(curOverlayitem);
			Log.d(TAG, "OverlayItem Add : " + curRes.getName() + " (" + curLatitude + ", " + curLongitude + ")");
		}
//		OverlayItem test = new OverlayItem(new GeoPoint(37551367, 126926296), "ABC", "ABC");
//		mItemizedOverlay.addOverlay(test);
//		
//		OverlayItem overlayitemG = new OverlayItem(new GeoPoint(37551367, 126926296), "학생회관", "G동");
//		mItemizedOverlay.addOverlay(overlayitemG);
//		OverlayItem overlayitemT = new OverlayItem(new GeoPoint(37550068, 126925166), "컴공/산공", "T동");
//		mItemizedOverlay.addOverlay(overlayitemT);
//		OverlayItem overlayitemC = new OverlayItem(new GeoPoint(37549468, 126926191), "인문사회관", "C동");
//		mItemizedOverlay.addOverlay(overlayitemC);
//		
		
		mapOverlays.add(mItemizedOverlay);
	}


	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	/** 클릭이벤트 **/
	@Override
	public void onClick(View v) {
		
	}

	/** 리스트아이템 클릭시 **/
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		Val.CUR_RES = categoryResultList.get(position);
		Intent intent = new Intent(CategoryResult.this, DetailWebView.class);
		startActivity(intent);
	}

	public void onResume(){
		super.onResume();
		if(myLocation == true){
			mLocation.enableMyLocation();
			mLocation.enableCompass();
		}
	}

	public void onPause(){
		super.onPause();
		mLocation.disableMyLocation();
		mLocation.disableCompass();
	}

	class MyLocationOverlay2 extends MyLocationOverlay{
		public MyLocationOverlay2(Context context, MapView mapView){
			super(context, mapView);
		}

		protected boolean dispatchTap(){
			Toast.makeText(CategoryResult.this, "여기가 현재위치", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
}
