package com.anth.redtable.values;

public class Restaurant {
	String rest_id = null;
	String name = null;
	//int phoneNumber;
	String address = null;
	double rti = 99;
	String food_type = null;
	int review_number = 0;
	String detail_link = null;
	double latitude = 0;
	double longitude = 0;
	
	public Restaurant(String temp){
	}
	public Restaurant(String mRest_id, String mName, String mAddress, double mRti, String mFood_type, int mReview_number, String mDetail_link, double mLatitude, double mLongitude){
		rest_id = mRest_id;
		name = mName;
		//phoneNumber = mPhoneNumber;
		address = mAddress;
		rti = mRti;
		food_type = mFood_type;
		review_number = mReview_number;
		detail_link = mDetail_link;
		latitude = mLatitude;
		longitude = mLongitude;
	}
	public String getRest_id() {
		return rest_id;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	public double getRti() {
		return rti;
	}
	public String getFood_type() {
		return food_type;
	}
	public int getReview_number() {
		return review_number;
	}
	public String getDetail_link() {
		return detail_link;
	}
	public double getLatitude() {
		return latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setRest_id(String rest_id) {
		this.rest_id = rest_id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setRti(double rti) {
		this.rti = rti;
	}
	public void setFood_type(String food_type) {
		this.food_type = food_type;
	}
	public void setReview_number(int review_number) {
		this.review_number = review_number;
	}
	public void setDetail_link(String detail_link) {
		this.detail_link = detail_link;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
}
