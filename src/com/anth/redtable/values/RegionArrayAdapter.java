package com.anth.redtable.values;

import java.util.ArrayList;

import com.anth.redtable.R;
import com.anth.redtable.R.id;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RegionArrayAdapter extends ArrayAdapter<Region>{
	private ArrayList<Region> mRegion;
	private Context mContext;
	private int mResource;
	private LayoutInflater mInflater;
	
	public RegionArrayAdapter(Context context, int LayoutResource, ArrayList<Region> region){
		super(context, LayoutResource, region);
		this.mRegion = region;
		this.mContext = context;
		this.mResource = LayoutResource;
		this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		Region mCurRegion = mRegion.get(position);
		if(convertView == null) convertView = mInflater.inflate(mResource, null);
		
		TextView leftMenuRegion = (TextView) convertView.findViewById(R.id.leftMenuRegion);
		leftMenuRegion.setText(mCurRegion.getRegion());
		
		return convertView;
	}
}
