package com.anth.redtable.values;

public class Region {
	private String region;
	private String regionCode1;
	private String regionCode2;
	
	public Region(String mRegion, String mRegionCode1, String mRegionCode2){
		region = mRegion;
		regionCode1 = mRegionCode1;
		regionCode2 = mRegionCode2;
	}

	public String getRegion() {
		return region;
	}

	public String getRegionCode1() {
		return regionCode1;
	}

	public String getRegionCode2() {
		return regionCode2;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setRegionCode1(String regionCode1) {
		this.regionCode1 = regionCode1;
	}

	public void setRegionCode2(String regionCode2) {
		this.regionCode2 = regionCode2;
	}
}
