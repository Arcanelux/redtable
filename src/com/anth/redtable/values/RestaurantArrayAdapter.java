package com.anth.redtable.values;

import java.util.ArrayList;

import com.anth.redtable.R;
import com.anth.redtable.R.drawable;
import com.anth.redtable.R.id;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class RestaurantArrayAdapter extends ArrayAdapter<Restaurant>{
	private ArrayList<Restaurant> mResList;
	private Context mContext;
	private int mResource;
	private LayoutInflater mInflater;

	public RestaurantArrayAdapter(Context context, int LayoutResource, ArrayList<Restaurant> resList){
		super(context, LayoutResource, resList);
		this.mResList = resList;
		this.mContext = context;
		this.mResource = LayoutResource;
		this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		if(convertView == null) convertView = mInflater.inflate(mResource, null);
		Restaurant mRestaurant = mResList.get(position);
		Log.i("position", "Position:" + position);

		TextView resCellName = (TextView) convertView.findViewById(R.id.resCellName);
		TextView resCellRTI = (TextView) convertView.findViewById(R.id.resCellRTI);
		TextView resCellReviews = (TextView) convertView.findViewById(R.id.resCellReviews);
		RatingBar resCellRatingBar = (RatingBar) convertView.findViewById(R.id.rbResCell);

		resCellName.setText(mRestaurant.name);
		resCellRTI.setText(Double.toString(mRestaurant.rti));
		resCellReviews.setText(Integer.toString(mRestaurant.review_number));

		/** rti에 따라 Mark이미지 지정 **/
		resCellRatingBar.setIsIndicator(true);
		resCellRatingBar.setRating((float)mResList.get(position).getRti()/2);
		resCellRatingBar.setFocusable(false);
		
		return convertView;
	}
}
