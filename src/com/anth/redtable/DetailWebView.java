package com.anth.redtable;

import com.anth.redtable.function.Val;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class DetailWebView extends Activity{
	WebView mWebView;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.detail_webview);
		
		Intent intent = getIntent();
		String url = intent.getStringExtra("url");
		if(url==null){
			url = Val.CUR_RES.getDetail_link();
		}
		mWebView = (WebView) findViewById(R.id.detailWebView);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.loadUrl(url);
		mWebView.setWebViewClient(new mWebViewClient());
		mWebView.getSettings().setDomStorageEnabled(true);
		//왠지모르겠지만 트위터 표시할려면 추가해줘야됨
		mWebView.setHorizontalScrollBarEnabled(false); // 세로 scroll 제거
		mWebView.setVerticalScrollBarEnabled(false); // 가로 scroll 제거
	}

	private class mWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
	        mWebView.goBack();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
}